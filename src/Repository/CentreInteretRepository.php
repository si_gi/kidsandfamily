<?php
/**
 * Created by PhpStorm.
 * User: si_gi
 * Date: 08/02/2019
 * Time: 11:05
 */
//src/Repository/CentreInteretRepository.php
namespace App\Repository;

use App\Entity\CentreInteret;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
/**
 * @method CentreInteret|null find($id, $lockMode = null, $lockVersion = null)
 * @method CentreInteret|null findOneBy(array $criteria, array $orderBy = null)
 * @method CentreInteret[]    findAll()
 * @method CentreInteret[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CentreInteretRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CentreInteret::class);
    }
}
