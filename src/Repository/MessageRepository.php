<?php
/**
 * Created by PhpStorm.
 * User: si_gi
 * Date: 08/02/2019
 * Time: 11:05
 */

namespace App\Repository;
//src/Repository/MessageRepository.php
use App\Entity\Message;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
/**
 * @method Message|null find($id, $lockMode = null, $lockVersion = null)
 * @method Message|null findOneBy(array $criteria, array $orderBy = null)
 * @method Message[]    findAll()
 * @method Message[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MessageRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Message::class);
    }

    /**
     * @return array
     */
    public function getConv($receiver, $sender){
        $entityManager = $this->getEntityManager();
        $queryBuilder = $entityManager->
        createQuery('SELECT p 
                            FROM App\Entity\Message p
                            WHERE p.receiver = :receiver AND p.sender = :sender 
                            OR p.receiver= :sender AND p.sender = :receiver
                            
                            ')
                    ->setParameter('receiver', $receiver)
                    ->setParameter('sender', $sender);
             return $queryBuilder->execute();
    }


}