<?php
/**
 * Created by PhpStorm.
 * User: si_gi
 * Date: 18/02/2019
 * Time: 09:50
 */

namespace App\Form;

use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityManagerInterface;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class SearchType extends AbstractType
{
    /** @var EntityManagerInterface */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;

    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $articles = $this->entityManager->getRepository(\App\Entity\BlogPost::class)->findAll();
            $nom = $interet->getNom();
            $builder->add($nom, CheckboxType::class, [
                    'label' => $nom,
                ]
            );
        $builder->add('submit', SubmitType::class,
            [
                'attr' => ['class' => 'form-control btn-primary pull-right'],
                'label' => 'Confirmer'
            ]);
    }
    public function getName()
    {
        return 'nom';
    }
}