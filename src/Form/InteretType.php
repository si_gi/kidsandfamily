<?php
/**
 * Created by PhpStorm.
 * User: si_gi
 * Date: 13/02/2019
 * Time: 08:48
 */

namespace App\Form;


use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\CentreInteret;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class InteretType extends AbstractType
{
    /** @var EntityManagerInterface */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;

    }
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $interets = $this->entityManager->getRepository(\App\Entity\CentreInteret::class)->findAll();
       foreach ($interets as $interet){
           $nom = $interet->getNom();
           $builder->add($nom, CheckboxType::class, [
                   'label' => $nom,
               ]
           );
      }
      $builder->add('submit', SubmitType::class,
          [
              'attr' => ['class' => 'form-control btn-primary pull-right'],
              'label' => 'Confirmer'
          ]);
    }
    public function getName()
    {
        return 'nom';
    }
}