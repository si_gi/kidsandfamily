<?php
/**
 * Created by PhpStorm.
 * User: si_gi
 * Date: 06/02/2019
 * Time: 08:07
 */

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CommentRepository")
 * @ORM\Table(name="comment")
 * @ORM\HasLifecycleCallbacks()
 */
class Comment
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var User
    * @ORM\ManyToOne(targetEntity="User")
    * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
    */
    protected $user;

    /**
     * @ORM\Column(type="text")
     */
    protected $comment;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $approved;

    /**
     * @ORM\ManyToOne(targetEntity="BlogPost")
     * @ORM\JoinColumn(name="blog_id", referencedColumnName="id")
     */
    protected $blog;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $created;

    /**
     * @ORM\Column(type="datetime", nullable= true)
     */
    protected $updated;

    /**
     * @var \Boolean
     * @ORM\Column(name="signaled", type="boolean", nullable=true)
     */
    private $signaled;

    public function __construct()
    {

        $this->setApproved(true);
    }

    /**
     * @return int
     */
    public function getId(){
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function setBlog($blog){
        $this->blog = $blog;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBlog(){
        return $this->blog;
    }

    /**
     * @param $user
     * @return BlogPost
     */
    public function setUser($user){
        $this->user = $user;
        return $this;
    }

    /**
     * @return User
     * @return Author
     */
    public function getUser(){
        return $this->user;
    }

    /**
     * @param $comment
     */
    public function setComment($comment){
        $this->comment = $comment;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getComment(){
        return $this->comment;
    }

    public function setApproved(){
        $this->approved = true;
    }

    /**
     * @return boolean
     */
    public function isApproved(){
        return $this->approved;
    }


    public function setSignaled($boolean){
        $this->signaled = (bool) $boolean;
    }


    public function isSignaled(){
        return $this->signaled;
    }

    /**
     * Set createdAt
     * @param \DateTime $createdAt
     * @return BlogPost
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     * @param \DateTime $updatedAt
     * @return BlogPost
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}