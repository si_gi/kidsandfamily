<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190129141633 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE image DROP url, DROP tempFilename, CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE alt alt VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE fos_user ADD cover_id INT DEFAULT NULL, DROP profile_picture');
        $this->addSql('ALTER TABLE fos_user ADD CONSTRAINT FK_957A6479922726E9 FOREIGN KEY (cover_id) REFERENCES image (id)');
        $this->addSql('CREATE INDEX IDX_957A6479922726E9 ON fos_user (cover_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE fos_user DROP FOREIGN KEY FK_957A6479922726E9');
        $this->addSql('DROP INDEX IDX_957A6479922726E9 ON fos_user');
        $this->addSql('ALTER TABLE fos_user ADD profile_picture LONGBLOB DEFAULT NULL, DROP cover_id');
        $this->addSql('ALTER TABLE image ADD url VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, ADD tempFilename VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE id id BIGINT AUTO_INCREMENT NOT NULL, CHANGE alt alt VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci');
    }
}
