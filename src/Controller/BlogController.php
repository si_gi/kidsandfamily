<?php
namespace App\Controller;

use App\Entity\BlogPost;
use App\Entity\Comment;
use App\Entity\User;
use App\Form\CommentType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class BlogController extends AbstractController
{
    /** @var integer */
    const POST_LIMIT = 5;
    /** @var EntityManagerInterface */
    private $entityManager;
    /** @var \Doctrine\Common\Persistence\ObjectRepository */
    private $userRepository;
    /** @var \Doctrine\Common\Persistence\ObjectRepository */
    private $blogPostRepository;
    /** @var \Doctrine\Common\Persistence\ObjectRepository */
    private $commentRepository;
    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->blogPostRepository = $entityManager->getRepository(BlogPost::class);
        $this->UserRepository = $entityManager->getRepository(User::class);
        $this->CommentRepository = $entityManager->getRepository(Comment::class);
    }
    /**
     * @Route("/articles", name="articles")
     * Affiche la liste des Articles
     */
    public function articlesActions(Request $request)
    {
        $page = 1;
        if ($request->get('page')) {
            $page = $request->get('page');
        }
        $articles = $this->blogPostRepository->getAllPosts($page, self::POST_LIMIT);
        // Verifie si l'article est désactivé ou non par un administrateur.
        // Si oui, alors l'article ne s'affiche pas
        foreach ($articles as $article){
            if(!$article->isEnabled()){
                unset($articles[array_search($article, $articles)]);
            }
        }
        return $this->render('blog/article_content.html.twig', [
            'blogPosts' => $articles,
            'totalBlogPosts' => $this->blogPostRepository->getPostCount(),
            'page' => $page,
            'entryLimit' => self::POST_LIMIT
        ]);
    }
    /**
     * @Route("/article/{slug}", name="article")
     * Affiche un article selon son slug(url unique)
     */
    public function articleAction(Request $request, $slug)
    {
        //Affichage de l'article //
        $blogPost = $this->blogPostRepository->findOneBySlug($slug);


            // Dans le cas où un article est désactivé, mais que l'utilisateur accède au lien quand même
            if(!$blogPost->isEnabled()){
                $this->addFlash("error", "Poste désactivé pour non respect de la charte éthique");
               return $this->redirectToRoute('articles');
            }
            $author_id= $blogPost->getAuthor();
            $author = $this->UserRepository->findOneById($author_id);
            if (!$blogPost) {
                $this->addFlash('error', 'Unable to find articles.html.twig!');
                return $this->redirectToRoute('articles');
            }

            // Affichage des commentaires liés à l'article //
        $commentaires = $this->CommentRepository->findByBlog($blogPost);
        foreach ($commentaires as $commentaire){
            if(!$commentaire->isApproved()
                || $commentaire->getBlog() != $blogPost){
                //unset retire un element d'un tableau (voir la doc php pour plus d'infos
                unset($commentaires[array_search($commentaire, $commentaires)]);
            }
        }
            // Formulaire de commentaire //
        $Comment = new Comment();
        $user = $this->UserRepository->findOneByUsername($this->getUser()->getUserName());
        $Comment->setUser($user);
        $Comment->setBlog($blogPost);
        $Comment->setApproved(true);
        $form = $this->createForm(CommentType::class, $Comment);
        $form->handleRequest($request);

        // Check is valid
        if ($form->isSubmitted() && $form->isValid()) {

            $this->entityManager->persist($Comment);

            $this->entityManager->flush($Comment);

            $this->addFlash('success', 'Commentaire posté');
            return $this->redirectToRoute('index');
        }

            return $this->render('blog/article.html.twig', array(
                'blogPost' => $blogPost,
                'user' => $author,
                'form' => $form->createView(),
                'commentaires' => $commentaires
            ));
    }

    /**
     * Retourne une liste de commentaires associé à un article
     */
    public function commentList($blogId)
    {
        return $this->render('blog/articles.html.twig', [
            'commentaires' => $commentaires,
        ]);
    }
    /**
     * ATTENTION, PAS ENCORE IMPLANTE
     * Le but de cette methode est de voir les articles publiés par un utilisateur
     * @Route("/user/{name}", name="user")
     */
    public function userAction($name)
    {
        $user = $this->userRepository->findOneByUsername($name);
        if (!$user) {
            $this->addFlash('error', 'Unable to find user!');
            return $this->redirectToRoute ('articles');
        }
        return $this->render('blog/user.html.twig', [
            'user' => $user
        ]);
    }
}
