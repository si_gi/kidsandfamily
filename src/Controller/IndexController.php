<?php
/**
 * Created by PhpStorm.
 * User: si_gi
 * Date: 23/01/2019
 * Time: 09:35
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class IndexController extends AbstractController
{
    /**
     * @Route("/homepage", name="homepage")
     */
    public function index() {
        if($this->getUser()){
            return $this->redirectToRoute("articles");
        }
        else{
            return $this->redirectToRoute("fos_user_security_login");
        }

    }
}
