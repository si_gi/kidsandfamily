<?php

namespace App\Controller;


use App\Entity\Contacts;
use App\Repository\CentreInteretRepository;
use App\Repository\MessageRepository;
use PhpParser\Node\Expr\Array_;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\User;
use App\Form\ProfileForm;
use App\Entity\BlogPost;
use App\Form\ArticleFormType;
use App\Entity\Comment;
use App\Form\CommentType;
use App\Entity\Message;
use App\Form\MessageType;
use App\Form\InteretType;
use App\Entity\CentreInteret;

/**
 * Class UserController
 * @package App\Controller
 * @Route("/article", name="article")
 */
class UserController extends AbstractController
{

    /** @var EntityManagerInterface */
    private $entityManager;
    /** @var \Doctrine\Common\Persistence\ObjectRepository */
    private $UserRepository;
    /** @var \Doctrine\Common\Persistence\ObjectRepository */
    private $blogPostRepository;
    /** @var \Doctrine\Common\Persistence\ObjectRepository */
    private $commentRepository;
    /** @var \Doctrine\Common\Persistence\ObjectRepository */
    private $messageRepository;
    /** @var \Doctrine\Common\Persistence\ObjectRepository */
    private $ContactsRepository;
    /** @var \Doctrine\Common\Persistence\ObjectRepository */
    private $CentreInteretRepository;



    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->blogPostRepository = $entityManager->getRepository(BlogPost::class);
        $this->UserRepository = $entityManager->getRepository(User::class);
        $this->commentRepository = $entityManager->getRepository(Comment::class);
        $this->messageRepository = $entityManager->getRepository(Message::class);
        $this->ContactsRepository = $entityManager->getRepository(Contacts::class);
        $this->CentreInteretRepository = $entityManager->getRepository(\App\Entity\CentreInteret::class);

    }
    
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Route("/profil/interet", name="interetUpdate")
     */
    public function Interet(Request $request){
        $user= $this->UserRepository->findByUsername($this->getUser()->getUserName());
        $interets= $this->CentreInteretRepository->findAll();
        $form = $this->createForm(InteretType::class, $user);
        $form->handleRequest($request);

        //var_dump($interets);
        // Check is valid
        if ($form->isSubmitted() && $form->isValid()) {
            foreach($interets as $interet){
                $user[0]->addInteret($interet->getId());

                $this->entityManager->persist($user[0]);

                $this->entityManager->flush($user[0]);
            }
            $this->addFlash('success', 'Merci de votre participation');

            return $this->redirectToRoute('index');
        }

        return $this->render('InteretForm.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/create-articles", name="user_create_entry")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createArticleAction(Request $request)
    {
        $blogPost = new BlogPost();

        $user = $this->UserRepository->findOneByUsername($this->getUser()->getUserName());

        $blogPost->setUser($user);
        $blogPost->setEnabled(true);


        $form = $this->createForm(ArticleFormType::class, $blogPost);
        $form->handleRequest($request);

        // Check is valid
        if ($form->isSubmitted() && $form->isValid()) {

            str_replace(" ","%20",$blogPost->getAdress());
            $this->entityManager->persist($blogPost);

            $this->entityManager->flush($blogPost);

            $this->addFlash('success', 'Merci de votre participation');

            $blogPost->setSlug();
            $this->entityManager->persist($blogPost);
            $this->entityManager->flush($blogPost);

            return $this->redirectToRoute('index');
        }

        return $this->render('article_form.html.twig', [
            'form' => $form->createView()
        ]);
    }
    /**
     * @param Request $request
     * @param String $slug
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Route("/article/{slug}", name="commentaire")
     */
    public function createCommentAction(Request $request, $slug)
    {
        $Comment = new Comment();

        $user = $this->UserRepository->findOneByUsername($this->getUser()->getUserName());
        $Comment->setUser($user);
        $blog = $this->blogPostRepository->findOneBySlug($slug);
        
        $Comment->setBlog($blog);
        $Comment->setApproved(true);
        
        $form = $this->createForm(CommentType::class, $Comment);
        $form->handleRequest($request);

        // Check is valid
        if ($form->isSubmitted() && $form->isValid()) {

            $this->entityManager->persist($Comment);

            $this->entityManager->flush($Comment);

            $this->addFlash('success', 'Commentaire posté');
            return $this->redirectToRoute('index');
        }

        return $this->render('blog/comment/form.html.twig', [
            'form' => $form->createView()
        ]);
    }




    public function signalArticle($slug){
        $blogPost = $this->blogPostRepository->findOneBySlug($slug);
        $blogPost->setIsSignaled(true);
        $this->entityManager->persist($blogPost);
        $this->entityManager->flush($blogPost);
        $this->addFlash('error', "L'article a bien était signalé");

        return $this->redirectToRoute("articles");
    }

    public function signalComment($id){
        $blogPost = $this->commentRepository->findOneById($id);
        $blogPost->setSignaled(true);
        $this->entityManager->persist($blogPost);
        $this->entityManager->flush($blogPost);
        $this->addFlash('error', "L'article a bien était signalé");

        return $this->redirectToRoute("articles");
    }

    /**
     * @Route("/article/voir", name="user_index")
     * @Route("/articles", name="user_articles")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function articlesAction()
    {
        $user= $this->UserRepository->findOneByUsername($this->getUser()->getUserName());

        $blogPosts = [];

        if ($user) {
            $blogPosts = $this->blogPostRepository->findByAuthor($user);
        }

        return $this->render('blog/articles.html.twig', [
            'blogPosts' => $blogPosts
        ]);
    }

    /**
     * @Route("/delete-articles/{entryId}", name="user_delete_entry")
     *
     * @param $entryId
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteArticleAction($entryId)
    {
        $blogPost = $this->blogPostRepository->findOneById($entryId);
        $author = $this->UserRepository->findOneByUsername($this->getUser()->getUserName());
        if (!$blogPost || $author !== $blogPost->getAuthor()) {
            $this->addFlash('error', 'Unable to remove articles.html.twig!');
            return $this->redirectToRoute('user_articles');
        }
        $this->entityManager->remove($blogPost);
        $this->entityManager->flush();
        $this->addFlash('success', 'Entry was deleted!');
        return $this->redirectToRoute('user_articles');
    }

    /**
     * @Route("messages/{receiver}",name="messages_form")
     */
    public function sendMessage(Request $request, $receiver){

        $userSender = $this->UserRepository->findOneByUsername($this->getUser()->getUserName());
        $userReceiver = $this->UserRepository->findOneByUsername($receiver);
        $havecontact = $this->haveContact($userReceiver);

        $message = new Message;
        $date = new \DateTime();
        $message->setSendAt($date);
        $message->setSender($userSender);
        $message->setReceiver($userReceiver);
        $form = $this->createForm(MessageType::class, $message);
        $form->handleRequest($request);

        // Check is valid
        if ($form->isSubmitted() && $form->isValid()) {

            if($havecontact == false){
                $contact = new Contacts();
                $contact->setIdUser($userSender);
                $contact->setIdContact($userReceiver);
                $this->entityManager->persist($contact);
                $this->entityManager->flush($contact);

                $contact2 = new Contacts();
                $contact2->setIdUser($userReceiver);
                $contact2->setIdContact($userSender);
                $this->entityManager->persist($contact2);
                $this->entityManager->flush($contact2);

            }
            $this->entityManager->persist($message);

            $this->entityManager->flush($message);

            $this->addFlash('success', 'Message envoyé');
            return $this->redirectToRoute('conversation'.$receiver);
        }

        return $this->render('/Messages/Message_Form.html.twig', [
            'form' => $form->createView()
        ]);
    }

    // Verifie si les utilisateurs ont déjà communiqués ou non (pour le menu des contacts) Attention bug potentiel (à vérifier)
    public function haveContact($userReceiver){
        $haveContacts = $this->ContactsRepository->getContactWhere($this->getUser()->getUserName(),$userReceiver);
        if( $haveContacts )
            return true;
        else{
            return false;
        }
    }

    /**
     * @Route("conversation/{contact}", name="conversation")
     */
    public function seeMessages($contact,Request $request){
        
        $user= $this->getUser();
        $userSender = $this->UserRepository->findOneByUsername($user->getUserName());
        $contacts = $this->ContactsRepository->findByIdUser($user->getId());
        $receiver = $this->UserRepository->findOneByUsername($contact);
        $messages = $this->messageRepository->getConv($receiver, $user->getId());

        $message = new Message;
        $date = new \DateTime();
        $message->setSendAt($date);
        $message->setSender($userSender);
        $message->setReceiver($receiver);
        $form = $this->createForm(MessageType::class, $message);
        $form->handleRequest($request);

        // Check is valid
        if ($form->isSubmitted() && $form->isValid()) {

            $this->entityManager->persist($message);

            $this->entityManager->flush($message);

            $this->addFlash('success', 'Message envoyé');
            return $this->redirectToRoute('conversation', array("contact" => $receiver));
        }

        return $this->render('Messages/Message.html.twig',[
            "messages" => $messages,
            "user" => $userSender,
            "contact" => $receiver,
            'form' => $form->createView(),
            'list_contact' => $contacts,
        ]);
    }

    /**
     * @Route("profil/{username}", name="see_profil")
     */
    public function seeProfil($username){
        $profil = $this->UserRepository->findOneByUsername($username);
        $interets = $profil->getInterets();
        $nomInterets = array();
        foreach ($interets as $interet){
            $idInteret = $this->CentreInteretRepository->findOneById($interet);
            $nomInterets[] = $idInteret->getNom();
        }
        return $this->render('bundles/FOSUserBundle/profile/show.html.twig',[
          "user"  =>$profil,
          "interets" => $nomInterets
        ]);
    }

    /**
     * @Route("contacts",  name="contacts")
     */
    public function seeContact(){
        $contacts = $this->ContactsRepository->findByIdUser($this->getUser()->getId());
        //$contacts = $this->ContactsRepository->findByIdContact($this->getUser()->getId());

        return $this->render('/Messages/Contacts.html.twig',[
            'contacts' => $contacts,
        ]);
    }

}