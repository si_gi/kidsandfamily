/* ========================================================================
 * Map Generator script using Google Map API v3
 *
 * ========================================================================
 * 
 *
 * @author Ibrahim As'ad
 * @Date: 6 Feb,2015
 * mail me : ibra.asad@gmail.com
 *
 * ========================================================================
 */

var latlngs = [], map, geocoder, padHTML = '',
    directionDisplay,
    directionsService = new google.maps.DirectionsService();

function showAddress(coordinates,address, doShowInfo, title, zoom, doNotCenterAndZoom, circleRadius, showMarker, links) {
    // Map-Marker hinzufügen. Adresse oder GPS-Koordinaten können übergeben werden
    function showLatLng(result) {
        if (!result || !result.length) alert(strings.address_not_found + address);
        else {
            var latlng = result[0].geometry.location;
            latlngs.push(latlng);
            if (!doNotCenterAndZoom) centerAndZoom(latlngs, zoom);
            var marker = createMarker(latlng, circleRadius, showMarker);
            if (marker) {
                setInfoHTML(marker, address, title, links)
                // Zoom und Anzeige des Markers behindern sich ohne timeout gegenseitig
                if (doShowInfo) setTimeout(function () {
                    showInfo(marker);
                }, 250);
            }
        }
    }
    console.log(coordinates);
    if(coordinates != null && coordinates != "") {
        cArray = coordinates.split(',');
        if(cArray.length == 2) {
            if($.isNumeric(cArray[0]) && $.isNumeric(cArray[1])) {
                cLatLng = new google.maps.LatLng(cArray[0], cArray[1]);
                showLatLng([{'geometry': {'location': cLatLng}}]);
            }
            else {
                alert('Enter Valid Coordinates. E.g: 53.2734,-7.77832031');
            }
        }
        else {
            alert('Enter Valid Coordinates. E.g: 53.2734,-7.77832031');
        }
    }
    else {
        if (typeof address == 'object') {
            if (address.length == 2 && address.xa == null && address.za == null)
                var latlng = new google.maps.LatLng(address[0], address[1]);
            else latlng = address;
            showLatLng([{'geometry': {'location': latlng}}]);
        }
        else geocoder.geocode({
                'address': address,
                'region': 'de',
            },
            showLatLng
        );
    }
};

function setInfoHTML(marker, address, title, links) {
    var routingURI = ['https://maps.google.com/maps?hl=' + strings.country_code + '&q=',
        encodeURIComponent(address),
        '&ie=UTF8&hq=&hnear=', encodeURIComponent(address),
        '&z=14&iwloc=A&f=d&daddr=',
        encodeURIComponent(address)].join('');
    // (Strasse) ((?:(?:Landeskürzel)? PLZ Ort|Ort (?:Landeskürzel)? PLZ))

    if(address != undefined && address != "") {
        var addressParts = address.match(/(.*?),?\s+((?:(?:[A-Z]{1,2}\s*\-?\s*)?\d+\s+\D+|\D+\s+(?:[A-Z]{1,2}\s*\-?\s*)?\d+))$/i);
        if (!addressParts || addressParts.length != 3) addressParts = address.split(',');
        else addressParts.shift(0);


        var infoHTML = ['<div class="infoWindow"><strong>', title,
            '</strong>' + padHTML + '<br /><span>',
            addressParts.join(padHTML + '<br />'),
            '</span>',
            '<span class="infoWindowLinks"><br /><br /><a id="directionsLink" href="javascript:void(0)" target="_blank" onclick="toggleDirections(); return false">' + strings.directions_link_text + '</a>'];
    }
    else {
        console.log(title);
        var infoHTML = ['<div class="infoWindow"><strong>', title,
            '</strong>' + padHTML + '<br /><span>',
            '</span>',
            '<span class="infoWindowLinks"><br /><br /><a id="directionsLink" href="javascript:void(0)" target="_blank" onclick="toggleDirections(); return false">' + strings.directions_link_text + '</a>'];
    }
    if (links) {
        for (var i = 0; i < links.length; i++) {
            infoHTML.push(' | <a href="' + links[i].uri + '" target="_blank">' + links[i].text + '</a>');
        }
    }
    infoHTML.push('</span>');
    infoHTML.push('<form id="directions" action="#" method="post" onsubmit="calcRoute(this.elements[\'origin\'].value, \'' + address + '\'); return false" style="display: none; margin: 0; padding: 0;"><div><input type="text" name="origin" value="' + strings.directions_initial_value + '" style="width: auto" title="' + strings.directions_tooltip + '" /> <input type="submit" value="►" style="background: transparent; border: 0 none; -moz-border-radius: 3px; -webkit-border-radius: 3px; border-radius: 3px; color: #6261D8; margin: 0; outline: 0; padding: 0;" title="' + strings.directions_tooltip2 + '" /></div></form></div>');
    marker.value = infoHTML.join('');
};

function showInfo(marker, address) {
    if (marker.infowindow) {
        marker.infowindow.setContent(marker.value);
    }
    else {
        marker.infowindow = new google.maps.InfoWindow({
            content: marker.value
        });
        google.maps.event.addListener(marker.infowindow, 'closeclick', function (e) {
            if (window.fe && fe['info']) fe['info'].checked = marker.infowindow == null;
            marker.infowindow = null;
        });
    }
    marker.infowindow.open(map, marker);
};

function createMarker(latlng, circleRadius, showMarker) {
    // Marker erzeugen, bei Klick wird Info-Popup gezeigt/versteckt
    if (circleRadius)
        var circle = new google.maps.Circle({map: map,
            center: latlng,
            strokeOpacity: 0.5, // 0.0...1.0
            fillColor: "#00ff00",
            strokeWeight: 1,
            radius: circleRadius, // Meter
            strokeColor: '#006600',
            fillOpacity: 0.25}); // 0.0...1.0
    if (showMarker) {
        var marker = new google.maps.Marker({
            map: map,
            position: latlng
        });
        google.maps.event.addListener(marker, 'click', function (e) {
            if (window.fe && fe['info']) fe['info'].checked = this.infowindow == null;
            if (this.infowindow) {
                this.infowindow.close();
                this.infowindow = null;
            }
            else showInfo(this);
        });
        map.markers.push(marker);
        return marker;
    }
};


function centerAndZoom(latlngs, zoom) {
    // Karte in der Mitte einer Liste von GPS-Koordinaten zentrieren
    // latlng: an array of instances of GLatLng
    var latlngbounds = new google.maps.LatLngBounds();
    for (var i = 0; i < latlngs.length; i ++)
        latlngbounds.extend(latlngs[i]);
    map.setCenter(latlngbounds.getCenter());
    map.setZoom(zoom);
    if (latlngs.length > 1) map.fitBounds(latlngbounds);
};

function calcRoute(start, end) {
    var request = {
        origin: start,
        destination: end,
        travelMode: google.maps.DirectionsTravelMode.DRIVING
    };
    directionsService.route(request, function(response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
        }
        else alert(strings.address_not_found + (response.Qf ? response.Qf.origin : start));
    });
};

function toggleDirections() {
    document.getElementById('directionsLink').style.display = 'none';
    document.getElementById('directions').style.display = 'block';
    document.getElementById('directions').elements['origin'].focus();
    document.getElementById('directions').elements['origin'].select();
}

function initialize() {
    // Parameter aus Query-String ziehen
    var token, match, decoded, parts, search = location.search.substr(1).split('&'),
        address, title, zoom = 15, doShowInfo = false, showMarker = true, mapType = google.maps.MapTypeId.ROADMAP, circleRadius = 0, links = [],
        w = 0, h = 0,coordinates;
    for (var i = 0; i < search.length; i++) {
        token = search[i].split('=');
        if (token.length == 2) {
            try {
                decoded = decodeURIComponent(token[1]);
            }
            catch (e) {
                decoded = unescape(token[1]);
            }
        }
        if(token[0] == 'coord') {
            coordinates = token[1];
        }
        if (token[0] == 'q') {
            match = decoded.match(/(.+)\+\((.*)\)/);
            if (match) {
                address = match[1];
                if (match.length == 3) title = match[2];
            }
            else {
                match1 = decoded.match(/\((.*)\)/);
                if(match1) {
                    title = match1[1];
                }
            }
        }
        else if (token[0] == 't') {
            if (token[1] == 'k') mapType = google.maps.MapTypeId.SATELLITE;
            else if (token[1] == 'h') mapType = google.maps.MapTypeId.HYBRID;
            else if (token[1] == 'p') mapType = google.maps.MapTypeId.TERRAIN;
        }
        else if (token[0] == 'z') {
            zoom = parseInt(token[1]);
        }
        else if (token[0] == 'iwloc') {
            doShowInfo = token[1] == 'A';
        }
        else if (token[0] == 'circle') {
            circleRadius = parseInt(token[1]);
        }
        else if (token[0] == 'width') {
            w = parseInt(token[1]);
        }
        else if (token[0] == 'height') {
            h = parseInt(token[1]);
        }
        else if (token[0] == 'marker') {
            showMarker = parseInt(token[1]);
        }
        else if (token[0] == 'link') {
            parts = decoded.split('|');
            links.push({'text': parts[0], 'uri': parts[1]});
        }
    }

    // Routenplaner
    directionsDisplay = new google.maps.DirectionsRenderer();

    // Map
    map = new google.maps.Map(document.getElementById('map'));
    map.setMapTypeId(mapType);
    map.setOptions({
        mapTypeControlOptions: {
            style: w < 1024 ? google.maps.MapTypeControlStyle.DROPDOWN_MENU : google.maps.MapTypeControlStyle.DEFAULT
        }
    });
    map.markers = [];
    // Routenplaner für Map vorbereiten
    directionsDisplay.setMap(map);

    // Geocoder (Übersetzung Adresse -> LatLng)
    geocoder = new google.maps.Geocoder();
    // Adresse zeigen

    if (coordinates || address) {
        showAddress(coordinates, address, doShowInfo, title, zoom, false, circleRadius, showMarker, links);
    }
};

var strings = {
    address: 'Address',
    address_not_found: 'Address not found: ',
    country_code: 'en',
    directions_initial_value: 'Please enter address',
    directions_link_text: 'Directions',
    directions_tooltip: 'Please enter address and press the ENTER/RETURN key',
    directions_tooltip2: 'Get directions'
};

var fe, mapType_ab = {
    '': google.maps.MapTypeId.ROADMAP,
    'k': google.maps.MapTypeId.SATELLITE,
    'h': google.maps.MapTypeId.HYBRID,
    'p': google.maps.MapTypeId.TERRAIN
}, mapType_ba = {}, lastZoom = null;

function initMapType_ba() {
    for (var i in mapType_ab) {
        mapType_ba[mapType_ab[i]] = i;
    };
}

function singleAddress(doNotClearOverlays) {
    if (!doNotClearOverlays) {
        saveState = fe['info'].checked;
        for (var i = 0; i < map.markers.length; i ++) {
            if (map.markers[i].infowindow) {
                map.markers[i].infowindow.close();
                map.markers[i].infowindow = null;
            }
            map.markers[i].setVisible(false);
        }
        fe['info'].checked = saveState;
        map.markers = [];
    }
    latlngs = [];
    showAddress(fe['coordinates'].value,fe['address'].value, fe['info'].checked, fe['title'].value, parseInt(fe['zoom'].value), false, 0, true);
};

function generate_code() {
    var z = 5;
    texte = new Array(z);
    texte[0] = "Create a route on google maps"
    texte[1] = "Draw map route"
    texte[2] = "Create route map"
    texte[3] = "Map a route"
    texte[4] = "Plot a route map"

    var jetzt = new Date();
    var z=(jetzt.getSeconds())%z;
    var linkText = texte[z];
    var qcord = "";
    var address = encodeURIComponent(fe['address'].value),
        title = encodeURIComponent(fe['title'].value),
        zoom = map.getZoom() || parseInt(fe['zoom'].value),
        t = fe['t'].value,
        iwloc = fe['info'].checked ? 'A' : 'B',
        wstr = fe['autofit'].checked ? '100%': parseInt(fe['w'].value)+'px',
        w = fe['autofit'].checked ? '100%': parseInt(fe['w'].value),
        h = parseInt(fe['h'].value),
        coordinates = fe['coordinates'].value;
    src = "";

    if(coordinates != null && coordinates != "") {
        src = 'https://maps.google.com/maps?width=' + w +'&amp;height=' + h +'&amp;hl=en&amp;coord=' + coordinates + '&amp;q=' + address + '+(' + title + ')&amp;ie=UTF8&amp;t=' + t + '&amp;z=' + zoom + '&amp;iwloc=' + iwloc + '&amp;output=embed';
    }
    else {
        src = 'https://maps.google.com/maps?width=' + w +'&amp;height=' + h +'&amp;hl=en&amp;q=' + address + '+(' + title + ')&amp;ie=UTF8&amp;t=' + t + '&amp;z=' + zoom + '&amp;iwloc=' + iwloc + '&amp;output=embed';
    }
    //src = 'https://maps.google.de/maps?hl=de&amp;q=' + address + '+(' + title + ')&amp;ie=UTF8&amp;t=' + t + '&amp;z=' + zoom + '&amp;iwloc=' + iwloc + '&amp;output=embed',

    var html = ['<div style="width: ' + wstr + '"><iframe width="' + w + '" height="' + h + '" src="' + src + '" frameborder="0" scrolling="no" marginheight="0" marginwidth="0">',
        '<a href="https://www.maps.ie/map-my-route/">' + linkText + '</a>',
        '</iframe></div><br />'];
    // '<span style="font-size: 9px;">',
    // '<a href="',
    // '</span></div>'];
    jQuery('#code textarea').val(html.join(''));
};

function checkZoom(zoom) {
    if (map.getZoom() != null && map.getZoom() < zoom) {
        //var mapType = zoom == 21 ? "Satellitenbild" : "Karte";
        alert("No data available for this zoom level.");
    }
    lastZoom = map.getZoom();
};

function load() {
    fe = document.forms['map_generator'].elements;
    initMapType_ba();
    initialize();
    singleAddress(true);
    generate_code();
    google.maps.event.addListener(map, 'zoom_changed', function () {
        newLevel = map.getZoom();
        var option, hasZoomLevel;
        for (var i = 0; i < fe['zoom'].options.length; i++) {
            option = fe['zoom'].options[i];
            if (option.value == '' + newLevel) {
                hasZoomLevel = true;
                fe['zoom'].selectedIndex = i;
                break;
            }
        }
        if (!hasZoomLevel) {
            fe['zoom'].selectedIndex = 14;
        }
        generate_code();
    });
    google.maps.event.addListener(map, 'maptypeid_changed', function () {
        var option, hasMapType;
        for (var i = 0; i < fe['t'].options.length; i++) {
            option = fe['t'].options[i];
            if (option.value == mapType_ba[map.getMapTypeId()]) {
                hasMapType = true;
                fe['t'].selectedIndex = i;
                break;
            }
        }
        if (!hasMapType) {
            fe['t'].selectedIndex = 0;
        }
        checkZoom(lastZoom);
        generate_code();
    });
    /*jQuery('input#title').focus(function () {
        if (!jQuery('input#title').hasClass('focus')) jQuery(this).addClass('focus');
        setTimeout(function () {
            jQuery('input#title').change();
            if (jQuery('input#title').hasClass('focus')) jQuery('input#title').focus();
        }, 500);
    });
    jQuery('input#title').blur(function () {
        jQuery(this).removeClass('focus');
    });*/
    jQuery('input#title').change(function () {
        if (!map.markers.length) return;
        setInfoHTML(map.markers[0], fe['address'].value, fe['title'].value);
        if (fe['info'].checked) showInfo(map.markers[0]);
        //if (fe['info'].checked) jQuery('.infoWindow strong').html(fe['title'].value);
    });
    jQuery('input#address').change(function () {
        //if(fe['coordinates'] == 'null' || fe['coordinates'] == "") {
        singleAddress();
        //}
    });
    jQuery('input#coordinates').change(function () {
        singleAddress();
    });
    jQuery('select#t').change(function () {
        lastZoom = map.getZoom();
        map.setMapTypeId(mapType_ab[this.value]);
        checkZoom(lastZoom);
    });
    jQuery('select#zoom').change(function () {
        //map.setZoom(parseInt(this.value));
        zoom = parseInt(this.value);
        centerAndZoom(latlngs, zoom);
        checkZoom(zoom);
        if (jQuery('input#info').get()[0].checked && map.markers.length) showInfo(map.markers[0]);
    });

    if(jQuery('input#autofit').attr('checked') == 'checked') {
        jQuery('input#w').val('100%').attr('disabled', 'disabled');
    }
    else {
        jQuery('input#w').attr('disabled', '');
    }

    jQuery('input#autofit').on('click',function () {
        if($(this).attr('class') == 'check') {
            jQuery('input#w').val('100%').attr('disabled', 'disabled');
            $(this).attr('class', 'uncheck');
            jQuery('#map').css('width', '100%');
        }
        else {
            jQuery('input#w').removeAttr('disabled', '').val('720');
            $(this).attr('class', 'check');
            jQuery('#map').css('width', '720px');
        }

        var w = jQuery('input#w') == '100%' ? '100%': jQuery('input#w').val()+'px';
        if (!map.markers.length) return;
        map.setOptions({
            mapTypeControlOptions: {
                style: w < 1024 ? google.maps.MapTypeControlStyle.DROPDOWN_MENU : google.maps.MapTypeControlStyle.DEFAULT
            }
        });
        google.maps.event.trigger(map, 'resize');
        if (fe['info'].checked) {
            map.markers[0].infowindow.close()
            map.markers[0].infowindow.open(map, map.markers[0]);
        }
        else map.setCenter(latlng[0]);
        zoom = parseInt(jQuery('select#zoom').val());
        centerAndZoom(latlngs, zoom);
    });

    jQuery('input#h, input#w').change(function () {
        var w = fe['w'].value == '100%' ? '100%' : parseInt(fe['w'].value) + 'px',
            h = parseInt(fe['h'].value);
        jQuery('#map').css('width', w);
        jQuery('#map').css('height', h + 'px');
        if (!map.markers.length) return;
        map.setOptions({
            mapTypeControlOptions: {
                style: w < 1024 ? google.maps.MapTypeControlStyle.DROPDOWN_MENU : google.maps.MapTypeControlStyle.DEFAULT
            }
        });
        google.maps.event.trigger(map, 'resize');
        if (fe['info'].checked) {
            map.markers[0].infowindow.close()
            map.markers[0].infowindow.open(map, map.markers[0]);
        }
        else map.setCenter(latlng[0]);
        zoom = parseInt(jQuery('select#zoom').val());
        centerAndZoom(latlngs, zoom);
    });
    jQuery('input#info').change(function () {
        if (!map.markers.length) return;
        if (this.checked) showInfo(map.markers[0]);
        else if (map.markers[0].infowindow) {
            map.markers[0].infowindow.close();
            map.markers[0].infowindow = null;
        }
    });
    jQuery('input, select').change(generate_code);
    // Update Map
    jQuery('select#t').change();
    jQuery('select#zoom').change();
    jQuery('input#h, input#w').change();
}