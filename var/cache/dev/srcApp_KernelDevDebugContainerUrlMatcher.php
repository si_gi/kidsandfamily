<?php

use Symfony\Component\Routing\Matcher\Dumper\PhpMatcherTrait;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class srcApp_KernelDevDebugContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    use PhpMatcherTrait;

    public function __construct(RequestContext $context)
    {
        $this->context = $context;
        $this->staticRoutes = [
            '/homepage' => [[['_route' => 'homepage', '_controller' => 'App\\Controller\\IndexController::index'], null, null, null, false, false, null]],
            '/article/profil/interet' => [[['_route' => 'articleinteretUpdate', '_controller' => 'App\\Controller\\UserController::Interet'], null, null, null, false, false, null]],
            '/article/create-articles' => [[['_route' => 'articleuser_create_entry', '_controller' => 'App\\Controller\\UserController::createArticleAction'], null, null, null, false, false, null]],
            '/article/articles' => [[['_route' => 'articleuser_articles', '_controller' => 'App\\Controller\\UserController::articlesAction'], null, null, null, false, false, null]],
            '/articlecontacts' => [[['_route' => 'articlecontacts', '_controller' => 'App\\Controller\\UserController::seeContact'], null, null, null, false, false, null]],
            '/admin' => [[['_route' => 'easyadmin', '_controller' => 'EasyCorp\\Bundle\\EasyAdminBundle\\Controller\\EasyAdminController::indexAction'], null, null, null, true, false, null]],
            '/_profiler' => [[['_route' => '_profiler_home', '_controller' => 'web_profiler.controller.profiler::homeAction'], null, null, null, true, false, null]],
            '/_profiler/search' => [[['_route' => '_profiler_search', '_controller' => 'web_profiler.controller.profiler::searchAction'], null, null, null, false, false, null]],
            '/_profiler/search_bar' => [[['_route' => '_profiler_search_bar', '_controller' => 'web_profiler.controller.profiler::searchBarAction'], null, null, null, false, false, null]],
            '/_profiler/phpinfo' => [[['_route' => '_profiler_phpinfo', '_controller' => 'web_profiler.controller.profiler::phpinfoAction'], null, null, null, false, false, null]],
            '/_profiler/open' => [[['_route' => '_profiler_open_file', '_controller' => 'web_profiler.controller.profiler::openAction'], null, null, null, false, false, null]],
            '/' => [[['_route' => 'index', '_controller' => 'App\\Controller\\IndexController::index'], null, null, null, false, false, null]],
            '/articles' => [[['_route' => 'articles', '_controller' => 'App\\Controller\\BlogController::articlesActions'], null, null, null, false, false, null]],
            '/contact' => [[['_route' => 'contacts', '_controller' => 'App\\Controller\\UserController:seeContact'], null, null, null, true, false, null]],
            '/article-form/create-article' => [[['_route' => 'user_create_article', '_controller' => 'App\\Controller\\UserController::createArticleAction'], null, null, null, true, false, null]],
            '/login' => [[['_route' => 'fos_user_security_login', '_controller' => 'fos_user.security.controller:loginAction'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
            '/login_check' => [[['_route' => 'fos_user_security_check', '_controller' => 'fos_user.security.controller:checkAction'], null, ['POST' => 0], null, false, false, null]],
            '/logout' => [[['_route' => 'fos_user_security_logout', '_controller' => 'fos_user.security.controller:logoutAction'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
            '/profile' => [[['_route' => 'fos_user_profile_show', '_controller' => 'fos_user.profile.controller:showAction'], null, ['GET' => 0], null, true, false, null]],
            '/profile/edit' => [[['_route' => 'fos_user_profile_edit', '_controller' => 'fos_user.profile.controller:editAction'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
            '/register' => [[['_route' => 'fos_user_registration_register', '_controller' => 'fos_user.registration.controller:registerAction'], null, ['GET' => 0, 'POST' => 1], null, true, false, null]],
            '/register/check-email' => [[['_route' => 'fos_user_registration_check_email', '_controller' => 'fos_user.registration.controller:checkEmailAction'], null, ['GET' => 0], null, false, false, null]],
            '/register/confirmed' => [[['_route' => 'fos_user_registration_confirmed', '_controller' => 'fos_user.registration.controller:confirmedAction'], null, ['GET' => 0], null, false, false, null]],
            '/resetting/request' => [[['_route' => 'fos_user_resetting_request', '_controller' => 'fos_user.resetting.controller:requestAction'], null, ['GET' => 0], null, false, false, null]],
            '/resetting/send-email' => [[['_route' => 'fos_user_resetting_send_email', '_controller' => 'fos_user.resetting.controller:sendEmailAction'], null, ['POST' => 0], null, false, false, null]],
            '/resetting/check-email' => [[['_route' => 'fos_user_resetting_check_email', '_controller' => 'fos_user.resetting.controller:checkEmailAction'], null, ['GET' => 0], null, false, false, null]],
            '/profile/change-password' => [[['_route' => 'fos_user_change_password', '_controller' => 'fos_user.change_password.controller:changePasswordAction'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        ];
        $this->regexpList = [
            0 => '{^(?'
                    .'|/user/([^/]++)(*:21)'
                    .'|/article(?'
                        .'|/(?'
                            .'|article/(?'
                                .'|([^/]++)(*:62)'
                                .'|voir(*:73)'
                            .')'
                            .'|delete\\-articles/([^/]++)(*:106)'
                            .'|signal/(?'
                                .'|([^/]++)(*:132)'
                                .'|commentaire/([^/]++)(*:160)'
                            .')'
                            .'|([^/]++)(*:177)'
                            .'|comment/([^/]++)(*:201)'
                        .')'
                        .'|messages/([^/]++)(*:227)'
                        .'|conversation/([^/]++)(*:256)'
                        .'|profil/([^/]++)(*:279)'
                    .')'
                    .'|/me(?'
                        .'|dia/cache/resolve/(?'
                            .'|([A-z0-9_-]*)/rc/([^/]++)/(.+)(*:345)'
                            .'|([A-z0-9_-]*)/(.+)(*:371)'
                        .')'
                        .'|ssage/([^/]++)(*:394)'
                    .')'
                    .'|/_(?'
                        .'|error/(\\d+)(?:\\.([^/]++))?(*:434)'
                        .'|wdt/([^/]++)(*:454)'
                        .'|profiler/([^/]++)(?'
                            .'|/(?'
                                .'|search/results(*:500)'
                                .'|router(*:514)'
                                .'|exception(?'
                                    .'|(*:534)'
                                    .'|\\.css(*:547)'
                                .')'
                            .')'
                            .'|(*:557)'
                        .')'
                    .')'
                    .'|/profil/([^/]++)(*:583)'
                    .'|/conversation/([^/]++)(*:613)'
                    .'|/re(?'
                        .'|gister/confirm/([^/]++)(*:650)'
                        .'|setting/reset/([^/]++)(*:680)'
                    .')'
                .')/?$}sDu',
        ];
        $this->dynamicRoutes = [
            21 => [[['_route' => 'user', '_controller' => 'App\\Controller\\BlogController::userAction'], ['name'], null, null, false, true, null]],
            62 => [[['_route' => 'articlecommentaire', '_controller' => 'App\\Controller\\UserController::createCommentAction'], ['slug'], null, null, false, true, null]],
            73 => [[['_route' => 'articleuser_index', '_controller' => 'App\\Controller\\UserController::articlesAction'], [], null, null, false, false, null]],
            106 => [[['_route' => 'articleuser_delete_entry', '_controller' => 'App\\Controller\\UserController::deleteArticleAction'], ['entryId'], null, null, false, true, null]],
            132 => [[['_route' => 'signal_article', '_controller' => 'App\\Controller\\UserController::signalArticle'], ['slug'], null, null, false, true, null]],
            160 => [[['_route' => 'signal_comment', '_controller' => 'App\\Controller\\UserController::signalComment'], ['id'], null, null, false, true, null]],
            177 => [[['_route' => 'article', '_controller' => 'App\\Controller\\BlogController::articleAction'], ['slug'], null, null, false, true, null]],
            201 => [[['_route' => 'user_create_comment', '_controller' => 'App\\Controller\\UserController::createCommentAction'], ['slug'], null, null, false, true, null]],
            227 => [[['_route' => 'articlemessages_form', '_controller' => 'App\\Controller\\UserController::sendMessage'], ['receiver'], null, null, false, true, null]],
            256 => [[['_route' => 'articleconversation', '_controller' => 'App\\Controller\\UserController::seeMessages'], ['contact'], null, null, false, true, null]],
            279 => [[['_route' => 'articlesee_profil', '_controller' => 'App\\Controller\\UserController::seeProfil'], ['username'], null, null, false, true, null]],
            345 => [[['_route' => 'liip_imagine_filter_runtime', '_controller' => 'Liip\\ImagineBundle\\Controller\\ImagineController::filterRuntimeAction'], ['filter', 'hash', 'path'], ['GET' => 0], null, false, true, null]],
            371 => [[['_route' => 'liip_imagine_filter', '_controller' => 'Liip\\ImagineBundle\\Controller\\ImagineController::filterAction'], ['filter', 'path'], ['GET' => 0], null, false, true, null]],
            394 => [[['_route' => 'message_form', '_controller' => 'App\\Controller\\UserController::sendMessage'], ['receiver'], null, null, false, true, null]],
            434 => [[['_route' => '_twig_error_test', '_controller' => 'twig.controller.preview_error::previewErrorPageAction', '_format' => 'html'], ['code', '_format'], null, null, false, true, null]],
            454 => [[['_route' => '_wdt', '_controller' => 'web_profiler.controller.profiler::toolbarAction'], ['token'], null, null, false, true, null]],
            500 => [[['_route' => '_profiler_search_results', '_controller' => 'web_profiler.controller.profiler::searchResultsAction'], ['token'], null, null, false, false, null]],
            514 => [[['_route' => '_profiler_router', '_controller' => 'web_profiler.controller.router::panelAction'], ['token'], null, null, false, false, null]],
            534 => [[['_route' => '_profiler_exception', '_controller' => 'web_profiler.controller.exception::showAction'], ['token'], null, null, false, false, null]],
            547 => [[['_route' => '_profiler_exception_css', '_controller' => 'web_profiler.controller.exception::cssAction'], ['token'], null, null, false, false, null]],
            557 => [[['_route' => '_profiler', '_controller' => 'web_profiler.controller.profiler::panelAction'], ['token'], null, null, false, true, null]],
            583 => [[['_route' => 'see_profil', '_controller' => 'App\\Controller\\UserController:seeProfil'], ['username'], null, null, false, true, null]],
            613 => [[['_route' => 'conversation', '_controller' => 'App\\Controller\\UserController::seeMessages'], ['contact'], null, null, false, true, null]],
            650 => [[['_route' => 'fos_user_registration_confirm', '_controller' => 'fos_user.registration.controller:confirmAction'], ['token'], ['GET' => 0], null, false, true, null]],
            680 => [[['_route' => 'fos_user_resetting_reset', '_controller' => 'fos_user.resetting.controller:resetAction'], ['token'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
        ];
    }
}
