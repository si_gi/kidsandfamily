<?php

use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Psr\Log\LoggerInterface;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class srcApp_KernelDevDebugContainerUrlGenerator extends Symfony\Component\Routing\Generator\UrlGenerator
{
    private static $declaredRoutes;
    private $defaultLocale;

    public function __construct(RequestContext $context, LoggerInterface $logger = null, string $defaultLocale = null)
    {
        $this->context = $context;
        $this->logger = $logger;
        $this->defaultLocale = $defaultLocale;
        if (null === self::$declaredRoutes) {
            self::$declaredRoutes = [
        'user' => [['name'], ['_controller' => 'App\\Controller\\BlogController::userAction'], [], [['variable', '/', '[^/]++', 'name', true], ['text', '/user']], [], []],
        'homepage' => [[], ['_controller' => 'App\\Controller\\IndexController::index'], [], [['text', '/homepage']], [], []],
        'articleinteretUpdate' => [[], ['_controller' => 'App\\Controller\\UserController::Interet'], [], [['text', '/article/profil/interet']], [], []],
        'articleuser_create_entry' => [[], ['_controller' => 'App\\Controller\\UserController::createArticleAction'], [], [['text', '/article/create-articles']], [], []],
        'articlecommentaire' => [['slug'], ['_controller' => 'App\\Controller\\UserController::createCommentAction'], [], [['variable', '/', '[^/]++', 'slug', true], ['text', '/article/article']], [], []],
        'articleuser_index' => [[], ['_controller' => 'App\\Controller\\UserController::articlesAction'], [], [['text', '/article/article/voir']], [], []],
        'articleuser_articles' => [[], ['_controller' => 'App\\Controller\\UserController::articlesAction'], [], [['text', '/article/articles']], [], []],
        'articleuser_delete_entry' => [['entryId'], ['_controller' => 'App\\Controller\\UserController::deleteArticleAction'], [], [['variable', '/', '[^/]++', 'entryId', true], ['text', '/article/delete-articles']], [], []],
        'articlemessages_form' => [['receiver'], ['_controller' => 'App\\Controller\\UserController::sendMessage'], [], [['variable', '/', '[^/]++', 'receiver', true], ['text', '/articlemessages']], [], []],
        'articleconversation' => [['contact'], ['_controller' => 'App\\Controller\\UserController::seeMessages'], [], [['variable', '/', '[^/]++', 'contact', true], ['text', '/articleconversation']], [], []],
        'articlesee_profil' => [['username'], ['_controller' => 'App\\Controller\\UserController::seeProfil'], [], [['variable', '/', '[^/]++', 'username', true], ['text', '/articleprofil']], [], []],
        'articlecontacts' => [[], ['_controller' => 'App\\Controller\\UserController::seeContact'], [], [['text', '/articlecontacts']], [], []],
        'easyadmin' => [[], ['_controller' => 'EasyCorp\\Bundle\\EasyAdminBundle\\Controller\\EasyAdminController::indexAction'], [], [['text', '/admin/']], [], []],
        'liip_imagine_filter_runtime' => [['filter', 'hash', 'path'], ['_controller' => 'Liip\\ImagineBundle\\Controller\\ImagineController::filterRuntimeAction'], ['filter' => '[A-z0-9_-]*', 'path' => '.+'], [['variable', '/', '.+', 'path', true], ['variable', '/', '[^/]++', 'hash', true], ['text', '/rc'], ['variable', '/', '[A-z0-9_-]*', 'filter', true], ['text', '/media/cache/resolve']], [], []],
        'liip_imagine_filter' => [['filter', 'path'], ['_controller' => 'Liip\\ImagineBundle\\Controller\\ImagineController::filterAction'], ['filter' => '[A-z0-9_-]*', 'path' => '.+'], [['variable', '/', '.+', 'path', true], ['variable', '/', '[A-z0-9_-]*', 'filter', true], ['text', '/media/cache/resolve']], [], []],
        '_twig_error_test' => [['code', '_format'], ['_controller' => 'twig.controller.preview_error::previewErrorPageAction', '_format' => 'html'], ['code' => '\\d+'], [['variable', '.', '[^/]++', '_format', true], ['variable', '/', '\\d+', 'code', true], ['text', '/_error']], [], []],
        '_wdt' => [['token'], ['_controller' => 'web_profiler.controller.profiler::toolbarAction'], [], [['variable', '/', '[^/]++', 'token', true], ['text', '/_wdt']], [], []],
        '_profiler_home' => [[], ['_controller' => 'web_profiler.controller.profiler::homeAction'], [], [['text', '/_profiler/']], [], []],
        '_profiler_search' => [[], ['_controller' => 'web_profiler.controller.profiler::searchAction'], [], [['text', '/_profiler/search']], [], []],
        '_profiler_search_bar' => [[], ['_controller' => 'web_profiler.controller.profiler::searchBarAction'], [], [['text', '/_profiler/search_bar']], [], []],
        '_profiler_phpinfo' => [[], ['_controller' => 'web_profiler.controller.profiler::phpinfoAction'], [], [['text', '/_profiler/phpinfo']], [], []],
        '_profiler_search_results' => [['token'], ['_controller' => 'web_profiler.controller.profiler::searchResultsAction'], [], [['text', '/search/results'], ['variable', '/', '[^/]++', 'token', true], ['text', '/_profiler']], [], []],
        '_profiler_open_file' => [[], ['_controller' => 'web_profiler.controller.profiler::openAction'], [], [['text', '/_profiler/open']], [], []],
        '_profiler' => [['token'], ['_controller' => 'web_profiler.controller.profiler::panelAction'], [], [['variable', '/', '[^/]++', 'token', true], ['text', '/_profiler']], [], []],
        '_profiler_router' => [['token'], ['_controller' => 'web_profiler.controller.router::panelAction'], [], [['text', '/router'], ['variable', '/', '[^/]++', 'token', true], ['text', '/_profiler']], [], []],
        '_profiler_exception' => [['token'], ['_controller' => 'web_profiler.controller.exception::showAction'], [], [['text', '/exception'], ['variable', '/', '[^/]++', 'token', true], ['text', '/_profiler']], [], []],
        '_profiler_exception_css' => [['token'], ['_controller' => 'web_profiler.controller.exception::cssAction'], [], [['text', '/exception.css'], ['variable', '/', '[^/]++', 'token', true], ['text', '/_profiler']], [], []],
        'index' => [[], ['_controller' => 'App\\Controller\\IndexController::index'], [], [['text', '/']], [], []],
        'articles' => [[], ['_controller' => 'App\\Controller\\BlogController::articlesActions'], [], [['text', '/articles']], [], []],
        'signal_article' => [['slug'], ['_controller' => 'App\\Controller\\UserController::signalArticle'], [], [['variable', '/', '[^/]++', 'slug', true], ['text', '/article/signal']], [], []],
        'signal_comment' => [['id'], ['_controller' => 'App\\Controller\\UserController::signalComment'], [], [['variable', '/', '[^/]++', 'id', true], ['text', '/article/signal/commentaire']], [], []],
        'article' => [['slug'], ['_controller' => 'App\\Controller\\BlogController::articleAction'], [], [['variable', '/', '[^/]++', 'slug', true], ['text', '/article']], [], []],
        'contacts' => [[], ['_controller' => 'App\\Controller\\UserController:seeContact'], [], [['text', '/contact/']], [], []],
        'see_profil' => [['username'], ['_controller' => 'App\\Controller\\UserController:seeProfil'], [], [['variable', '/', '[^/]++', 'username', true], ['text', '/profil']], [], []],
        'conversation' => [['contact'], ['_controller' => 'App\\Controller\\UserController::seeMessages'], [], [['variable', '/', '[^/]++', 'contact', true], ['text', '/conversation']], [], []],
        'message_form' => [['receiver'], ['_controller' => 'App\\Controller\\UserController::sendMessage'], [], [['variable', '/', '[^/]++', 'receiver', true], ['text', '/message']], [], []],
        'user_create_comment' => [['slug'], ['_controller' => 'App\\Controller\\UserController::createCommentAction'], [], [['variable', '/', '[^/]++', 'slug', true], ['text', '/article/comment']], [], []],
        'user_create_article' => [[], ['_controller' => 'App\\Controller\\UserController::createArticleAction'], [], [['text', '/article-form/create-article/']], [], []],
        'fos_user_security_login' => [[], ['_controller' => 'fos_user.security.controller:loginAction'], [], [['text', '/login']], [], []],
        'fos_user_security_check' => [[], ['_controller' => 'fos_user.security.controller:checkAction'], [], [['text', '/login_check']], [], []],
        'fos_user_security_logout' => [[], ['_controller' => 'fos_user.security.controller:logoutAction'], [], [['text', '/logout']], [], []],
        'fos_user_profile_show' => [[], ['_controller' => 'fos_user.profile.controller:showAction'], [], [['text', '/profile/']], [], []],
        'fos_user_profile_edit' => [[], ['_controller' => 'fos_user.profile.controller:editAction'], [], [['text', '/profile/edit']], [], []],
        'fos_user_registration_register' => [[], ['_controller' => 'fos_user.registration.controller:registerAction'], [], [['text', '/register/']], [], []],
        'fos_user_registration_check_email' => [[], ['_controller' => 'fos_user.registration.controller:checkEmailAction'], [], [['text', '/register/check-email']], [], []],
        'fos_user_registration_confirm' => [['token'], ['_controller' => 'fos_user.registration.controller:confirmAction'], [], [['variable', '/', '[^/]++', 'token', true], ['text', '/register/confirm']], [], []],
        'fos_user_registration_confirmed' => [[], ['_controller' => 'fos_user.registration.controller:confirmedAction'], [], [['text', '/register/confirmed']], [], []],
        'fos_user_resetting_request' => [[], ['_controller' => 'fos_user.resetting.controller:requestAction'], [], [['text', '/resetting/request']], [], []],
        'fos_user_resetting_send_email' => [[], ['_controller' => 'fos_user.resetting.controller:sendEmailAction'], [], [['text', '/resetting/send-email']], [], []],
        'fos_user_resetting_check_email' => [[], ['_controller' => 'fos_user.resetting.controller:checkEmailAction'], [], [['text', '/resetting/check-email']], [], []],
        'fos_user_resetting_reset' => [['token'], ['_controller' => 'fos_user.resetting.controller:resetAction'], [], [['variable', '/', '[^/]++', 'token', true], ['text', '/resetting/reset']], [], []],
        'fos_user_change_password' => [[], ['_controller' => 'fos_user.change_password.controller:changePasswordAction'], [], [['text', '/profile/change-password']], [], []],
    ];
        }
    }

    public function generate($name, $parameters = [], $referenceType = self::ABSOLUTE_PATH)
    {
        $locale = $parameters['_locale']
            ?? $this->context->getParameter('_locale')
            ?: $this->defaultLocale;

        if (null !== $locale && null !== $name) {
            do {
                if ((self::$declaredRoutes[$name.'.'.$locale][1]['_canonical_route'] ?? null) === $name) {
                    unset($parameters['_locale']);
                    $name .= '.'.$locale;
                    break;
                }
            } while (false !== $locale = strstr($locale, '_', true));
        }

        if (!isset(self::$declaredRoutes[$name])) {
            throw new RouteNotFoundException(sprintf('Unable to generate a URL for the named route "%s" as such route does not exist.', $name));
        }

        list($variables, $defaults, $requirements, $tokens, $hostTokens, $requiredSchemes) = self::$declaredRoutes[$name];

        return $this->doGenerate($variables, $defaults, $requirements, $tokens, $parameters, $name, $referenceType, $hostTokens, $requiredSchemes);
    }
}
