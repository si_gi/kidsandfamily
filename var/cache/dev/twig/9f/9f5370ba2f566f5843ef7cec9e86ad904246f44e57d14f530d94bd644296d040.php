<?php

/* blog/article.html.twig */
class __TwigTemplate_38a8f6d8baa695b9ffbc471393844366b6420cf60ddfe3ed6c2035c31450f6e2 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'body' => [$this, 'block_body'],
            'test' => [$this, 'block_test'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "blog/article.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "blog/article.html.twig"));

        // line 1
        $this->loadTemplate("layout.html.twig", "blog/article.html.twig", 1)->display($context);
        // line 2
        echo "


";
        // line 5
        $this->displayBlock('body', $context, $blocks);
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    ";
        $this->displayBlock('test', $context, $blocks);
        // line 75
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 6
    public function block_test($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "test"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "test"));

        // line 7
        echo "        ";
        if ((isset($context["blogPost"]) || array_key_exists("blogPost", $context))) {
            // line 8
            echo "            <p>test</p>
                <div>
                    ";
            // line 10
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 10, $this->source); })()), "username", []), "html", null, true);
            echo "
                </div>
                <div>

                    <div class=\"text\">

                        ";
            // line 16
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["blogPost"]) || array_key_exists("blogPost", $context) ? $context["blogPost"] : (function () { throw new Twig_Error_Runtime('Variable "blogPost" does not exist.', 16, $this->source); })()), "description", []), "html", null, true);
            echo "
                    </div>
                    <div>
                        ";
            // line 19
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["blogPost"]) || array_key_exists("blogPost", $context) ? $context["blogPost"] : (function () { throw new Twig_Error_Runtime('Variable "blogPost" does not exist.', 19, $this->source); })()), "createdAt", []), "Y-m-d"), "html", null, true);
            echo "
                    </div>
                    <div style=\"width: 100%\">
                        <iframe width=\"40%\" height=\"300\" src=\"
                        https://maps.google.com/maps?width=100%&amp;height=300&amp;hl=en&amp;q=
                        ";
            // line 24
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["blogPost"]) || array_key_exists("blogPost", $context) ? $context["blogPost"] : (function () { throw new Twig_Error_Runtime('Variable "blogPost" does not exist.', 24, $this->source); })()), "adress", []), "html", null, true);
            echo "&amp;ie=UTF8&amp;t=&amp;z=14&amp;iwloc=B&amp;output=embed\" frameborder=\"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\">
                            </iframe></div><br />
                    <div>
                        <a href=\"";
            // line 27
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("signal_article", ["slug" => twig_get_attribute($this->env, $this->source, (isset($context["blogPost"]) || array_key_exists("blogPost", $context) ? $context["blogPost"] : (function () { throw new Twig_Error_Runtime('Variable "blogPost" does not exist.', 27, $this->source); })()), "slug", [])]), "html", null, true);
            echo "\">Signaler</a>

                    </div>
                </div>
    <!-- AFFICHAGE DES COMMENTAIRES -->
                <div>
                    <table>
                        ";
            // line 34
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["commentaires"]) || array_key_exists("commentaires", $context) ? $context["commentaires"] : (function () { throw new Twig_Error_Runtime('Variable "commentaires" does not exist.', 34, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["commentaire"]) {
                // line 35
                echo "                            <tr>
                                <td><a href=\"";
                // line 36
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("see_profil", ["username" => twig_get_attribute($this->env, $this->source, $context["commentaire"], "user", [])]), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["commentaire"], "user", []), "html", null, true);
                echo "</a> :</td>
                            </tr>
                            <tr style=\"border-bottom: 3px solid black; padding: 50px;\">
                                <td>";
                // line 39
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["commentaire"], "comment", []), "html", null, true);
                echo "</td>
                                <td><a href=\"";
                // line 40
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("signal_comment", ["id" => twig_get_attribute($this->env, $this->source, $context["commentaire"], "id", [])]), "html", null, true);
                echo "\">Signaler</a></td>

                            </tr>




                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['commentaire'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 48
            echo "                    </table>
                </div>

                    <div class=\"row\">
                        <div class=\"col-sm-12 blog-main\">
                            ";
            // line 53
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 53, $this->source); })()), "flashes", []));
            foreach ($context['_seq'] as $context["label"] => $context["messages"]) {
                // line 54
                echo "                                ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($context["messages"]);
                foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                    // line 55
                    echo "                                    <div class=\"bg-";
                    echo twig_escape_filter($this->env, $context["label"], "html", null, true);
                    echo "\">
                                        ";
                    // line 56
                    echo twig_escape_filter($this->env, $context["message"], "html", null, true);
                    echo "
                                    </div>
                                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 59
                echo "                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['label'], $context['messages'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 60
            echo "                            ";
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 60, $this->source); })()), 'form_start');
            echo "
                            <div class=\"form-group col-md-4 pull-right\">
                                ";
            // line 62
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 62, $this->source); })()), "comment", []), 'widget');
            echo "
                            </div>
                            <div class=\"form-group col-md-4 pull-right\">
                                ";
            // line 65
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 65, $this->source); })()), "submit", []), 'widget');
            echo "
                            </div>
                            ";
            // line 67
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 67, $this->source); })()), 'form_end');
            echo "


                        </div>
                    </div>

        ";
        }
        // line 74
        echo "    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "blog/article.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  221 => 74,  211 => 67,  206 => 65,  200 => 62,  194 => 60,  188 => 59,  179 => 56,  174 => 55,  169 => 54,  165 => 53,  158 => 48,  144 => 40,  140 => 39,  132 => 36,  129 => 35,  125 => 34,  115 => 27,  109 => 24,  101 => 19,  95 => 16,  86 => 10,  82 => 8,  79 => 7,  70 => 6,  59 => 75,  56 => 6,  38 => 5,  33 => 2,  31 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% include \"layout.html.twig\" %}



{% block body %}
    {% block test %}
        {% if blogPost is defined %}
            <p>test</p>
                <div>
                    {{ user.username }}
                </div>
                <div>

                    <div class=\"text\">

                        {{ blogPost.description }}
                    </div>
                    <div>
                        {{ blogPost.createdAt|date('Y-m-d') }}
                    </div>
                    <div style=\"width: 100%\">
                        <iframe width=\"40%\" height=\"300\" src=\"
                        https://maps.google.com/maps?width=100%&amp;height=300&amp;hl=en&amp;q=
                        {{ blogPost.adress }}&amp;ie=UTF8&amp;t=&amp;z=14&amp;iwloc=B&amp;output=embed\" frameborder=\"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\">
                            </iframe></div><br />
                    <div>
                        <a href=\"{{ path('signal_article', {'slug': blogPost.slug }) }}\">Signaler</a>

                    </div>
                </div>
    <!-- AFFICHAGE DES COMMENTAIRES -->
                <div>
                    <table>
                        {% for commentaire in commentaires %}
                            <tr>
                                <td><a href=\"{{ path('see_profil', {\"username\": commentaire.user}) }}\">{{ commentaire.user }}</a> :</td>
                            </tr>
                            <tr style=\"border-bottom: 3px solid black; padding: 50px;\">
                                <td>{{ commentaire.comment }}</td>
                                <td><a href=\"{{ path('signal_comment', {'id': commentaire.id }) }}\">Signaler</a></td>

                            </tr>




                        {% endfor %}
                    </table>
                </div>

                    <div class=\"row\">
                        <div class=\"col-sm-12 blog-main\">
                            {% for label, messages in app.flashes %}
                                {% for message in messages %}
                                    <div class=\"bg-{{ label }}\">
                                        {{ message }}
                                    </div>
                                {% endfor %}
                            {% endfor %}
                            {{ form_start(form) }}
                            <div class=\"form-group col-md-4 pull-right\">
                                {{ form_widget(form.comment) }}
                            </div>
                            <div class=\"form-group col-md-4 pull-right\">
                                {{ form_widget(form.submit) }}
                            </div>
                            {{ form_end(form) }}


                        </div>
                    </div>

        {% endif %}
    {% endblock %}

{% endblock %}", "blog/article.html.twig", "C:\\laragon\\www\\kidsandfamily-master (1)\\kidsandfamily-master\\templates\\blog\\article.html.twig");
    }
}
