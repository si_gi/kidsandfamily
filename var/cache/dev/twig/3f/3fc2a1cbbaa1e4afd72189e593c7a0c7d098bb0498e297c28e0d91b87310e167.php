<?php

/* blog/article_content.html.twig */
class __TwigTemplate_f203bd54ba0c39b27186cd16d7b294112b63ecc14c274402f161b2a6033f3316 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "blog/article_content.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "blog/article_content.html.twig"));

        // line 1
        echo twig_include($this->env, $context, "/header.html.twig");
        echo "
<hr>
<div class=\"container padding col-md-12 text-center\">
  <h1>Bienvenu dans la rubrique Articles</h1>
</div>
<hr>

<div class=\"row padding\">
    <div class=\"col-lg-10\">
        <div class=\"container-fluid padding text-center\">
            <div class=\"row padding\">
    <div class=\"col-3\">

    </div>
    <div class=\"col-2\">
        <a href=\"";
        // line 16
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("user_create_article");
        echo "\"class=\"btn btn-outline-secondary\">Créer un article</a>
    </div>
                <div class=\"col-2\">
                    <a href=\"";
        // line 19
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("user_create_article");
        echo "\" class=\"btn btn-outline-secondary\">Je sais pas</a>
                </div>
                <div class=\"col-2\">
                    <a href=\"";
        // line 22
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("user_create_article");
        echo "\" class=\"btn btn-outline-secondary\">Je sais pas</a>
                </div>
                <div class=\"col-2\">
                    <a href=\"";
        // line 25
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("user_create_article");
        echo "\" class=\"btn btn-outline-secondary\">Je sais pas</a>
                </div>
</div>
    </div>
</div>
</div>
";
        // line 31
        if ((isset($context["blogPosts"]) || array_key_exists("blogPosts", $context))) {
            // line 32
            echo "<div class=\"container-fluid padding\">
    <div class=\"row padding\">
        <div class=\"col-10\">
        <div class=\"container-fluid padding\">
            <div class=\"row padding\">


                    ";
            // line 39
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["blogPosts"]) || array_key_exists("blogPosts", $context) ? $context["blogPosts"] : (function () { throw new Twig_Error_Runtime('Variable "blogPosts" does not exist.', 39, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["blogPost"]) {
                // line 40
                echo "
                        <div class=\"col-md-12\">
                            <a href=\"";
                // line 42
                echo twig_escape_filter($this->env, ("article/" . twig_get_attribute($this->env, $this->source, $context["blogPost"], "slug", [])), "html", null, true);
                echo "\">
                            <div class=\"media bg-light p-3\">
                                ";
                // line 44
                if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["blogPost"], "cover", [], "any", false, true), "file", [], "any", true, true)) {
                    // line 45
                    echo "                                    <img class=\"mr-3\" src=\"";
                    echo twig_escape_filter($this->env, $this->extensions['Liip\ImagineBundle\Templating\FilterExtension']->filter($this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("uploads/" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["blogPost"], "cover", []), "file", []))), "new_list"), "html", null, true);
                    echo "\" alt=\"Card image cap\" />
                                 ";
                }
                // line 47
                echo "                                <div class=\"media-body\">
                                    <h5 class=\"mt-0\"> ";
                // line 48
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["blogPost"], "title", []), "html", null, true);
                echo "</h5>
                                     <p>";
                // line 49
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["blogPost"], "description", []), "html", null, true);
                echo "</p>
                                 </div>
                            </div>
                            </a>
                        </div>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['blogPost'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 55
            echo "            </div>
            </div>
        </div>

</div>
</div>
    <div id=\"sidebar-wrapper\">
        <ul class=\"sidebar-nav\">
            <li class=\"sidebar-brand\"> <a href=\"#\"> Start Bootstrap </a> </li>
            <li> <a href=\"#\">Dashboard</a> </li>
            <li> <a href=\"#\">Shortcuts</a> </li>
            <li> <a href=\"#\">Overview</a> </li>
            <li> <a href=\"#\">Events</a> </li>
            <li> <a href=\"#\">About</a> </li>
            <li> <a href=\"#\">Services</a> </li>
            <li> <a href=\"#\">Contact</a> </li>
        </ul>
    </div> <!-- /#sidebar-wrapper -->

    <div>
    <nav class=\"navbar navbar-expand-md navbar-dark bg-primary fixed-right\" >
        <ul>
            <li>
                <a href=\"";
            // line 78
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("user_create_article");
            echo "\">Créer un article</a></li>

            <li><a href=\"#\">Je sais pas</a></li>
        </ul>


    </nav>
</div>

";
        }
        // line 88
        echo "
<hr class=\"my-4\">
";
        // line 90
        echo twig_include($this->env, $context, "/footer.html.twig");
        echo "
    <!--  <div class=\"col-md-12\">
          <div class=\"card mb-3\">
              <img class=\"card-img-top\" src=\"img/image6.jpg\" alt=\"Card image cap\">
              <div class=\"card-body\">
                  <h5 class=\"card-title\">Card title</h5>
                  <p class=\"card-text\">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                  <p class=\"card-text\"><small class=\"text-muted\">Last updated 3 mins ago</small></p>
              </div>
          </div>
      </div>-->
    <!--  <div class=\"card mb-3\">
      <img class=\"card-img-top\" src=\"img/image6.jpg\" alt=\"Card image cap\">
      <div class=\"card-body\">
          <h5 class=\"card-title\">Card title</h5>
          <p class=\"card-text\">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
          <p class=\"card-text\"><small class=\"text-muted\">Last updated 3 mins ago</small></p>
      </div>
  </div>-->";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "blog/article_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  167 => 90,  163 => 88,  150 => 78,  125 => 55,  113 => 49,  109 => 48,  106 => 47,  100 => 45,  98 => 44,  93 => 42,  89 => 40,  85 => 39,  76 => 32,  74 => 31,  65 => 25,  59 => 22,  53 => 19,  47 => 16,  29 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{include('/header.html.twig')}}
<hr>
<div class=\"container padding col-md-12 text-center\">
  <h1>Bienvenu dans la rubrique Articles</h1>
</div>
<hr>

<div class=\"row padding\">
    <div class=\"col-lg-10\">
        <div class=\"container-fluid padding text-center\">
            <div class=\"row padding\">
    <div class=\"col-3\">

    </div>
    <div class=\"col-2\">
        <a href=\"{{ path('user_create_article')  }}\"class=\"btn btn-outline-secondary\">Créer un article</a>
    </div>
                <div class=\"col-2\">
                    <a href=\"{{ path('user_create_article')  }}\" class=\"btn btn-outline-secondary\">Je sais pas</a>
                </div>
                <div class=\"col-2\">
                    <a href=\"{{ path('user_create_article')  }}\" class=\"btn btn-outline-secondary\">Je sais pas</a>
                </div>
                <div class=\"col-2\">
                    <a href=\"{{ path('user_create_article')  }}\" class=\"btn btn-outline-secondary\">Je sais pas</a>
                </div>
</div>
    </div>
</div>
</div>
{% if blogPosts is defined%}
<div class=\"container-fluid padding\">
    <div class=\"row padding\">
        <div class=\"col-10\">
        <div class=\"container-fluid padding\">
            <div class=\"row padding\">


                    {% for blogPost in blogPosts %}

                        <div class=\"col-md-12\">
                            <a href=\"{{ 'article/'~blogPost.slug}}\">
                            <div class=\"media bg-light p-3\">
                                {% if blogPost.cover.file is defined %}
                                    <img class=\"mr-3\" src=\"{{ asset('uploads/'~blogPost.cover.file)| imagine_filter('new_list') }}\" alt=\"Card image cap\" />
                                 {% endif %}
                                <div class=\"media-body\">
                                    <h5 class=\"mt-0\"> {{ blogPost.title }}</h5>
                                     <p>{{ blogPost.description }}</p>
                                 </div>
                            </div>
                            </a>
                        </div>
                    {% endfor %}
            </div>
            </div>
        </div>

</div>
</div>
    <div id=\"sidebar-wrapper\">
        <ul class=\"sidebar-nav\">
            <li class=\"sidebar-brand\"> <a href=\"#\"> Start Bootstrap </a> </li>
            <li> <a href=\"#\">Dashboard</a> </li>
            <li> <a href=\"#\">Shortcuts</a> </li>
            <li> <a href=\"#\">Overview</a> </li>
            <li> <a href=\"#\">Events</a> </li>
            <li> <a href=\"#\">About</a> </li>
            <li> <a href=\"#\">Services</a> </li>
            <li> <a href=\"#\">Contact</a> </li>
        </ul>
    </div> <!-- /#sidebar-wrapper -->

    <div>
    <nav class=\"navbar navbar-expand-md navbar-dark bg-primary fixed-right\" >
        <ul>
            <li>
                <a href=\"{{ path('user_create_article')  }}\">Créer un article</a></li>

            <li><a href=\"#\">Je sais pas</a></li>
        </ul>


    </nav>
</div>

{% endif %}

<hr class=\"my-4\">
{{include('/footer.html.twig')}}
    <!--  <div class=\"col-md-12\">
          <div class=\"card mb-3\">
              <img class=\"card-img-top\" src=\"img/image6.jpg\" alt=\"Card image cap\">
              <div class=\"card-body\">
                  <h5 class=\"card-title\">Card title</h5>
                  <p class=\"card-text\">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                  <p class=\"card-text\"><small class=\"text-muted\">Last updated 3 mins ago</small></p>
              </div>
          </div>
      </div>-->
    <!--  <div class=\"card mb-3\">
      <img class=\"card-img-top\" src=\"img/image6.jpg\" alt=\"Card image cap\">
      <div class=\"card-body\">
          <h5 class=\"card-title\">Card title</h5>
          <p class=\"card-text\">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
          <p class=\"card-text\"><small class=\"text-muted\">Last updated 3 mins ago</small></p>
      </div>
  </div>-->", "blog/article_content.html.twig", "C:\\laragon\\www\\kidsandfamily-master (1)\\kidsandfamily-master\\templates\\blog\\article_content.html.twig");
    }
}
