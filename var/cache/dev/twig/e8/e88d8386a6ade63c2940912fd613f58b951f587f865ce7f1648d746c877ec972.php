<?php

/* Messages/Contacts.html.twig */
class __TwigTemplate_95da2a146d58aebf0f5b38c699cf4ff14d1cb82c869ac9d47f9a333df64883f0 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "Messages/Contacts.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "Messages/Contacts.html.twig"));

        // line 1
        $this->loadTemplate("layout.html.twig", "Messages/Contacts.html.twig", 1)->display($context);
        // line 2
        echo "<script type=\"text/css\">
    #sidebar{
        position:fixed;
        padding-left: 0%;
        margin-left: 80%;
        padding-right: 0%;
        top: 0;
        bottom: 0;
    }
</script>
<nav id=\"sidebar\" >
<ul>
";
        // line 14
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["contacts"]) || array_key_exists("contacts", $context) ? $context["contacts"] : (function () { throw new Twig_Error_Runtime('Variable "contacts" does not exist.', 14, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["contact"]) {
            // line 15
            echo "    <li>
        <a href=\"";
            // line 16
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("conversation", ["contact" => twig_get_attribute($this->env, $this->source, $context["contact"], "idcontact", [])]), "html", null, true);
            echo "\" class=\"btn btn-warning\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["contact"], "idContact", []), "html", null, true);
            echo "</a>

    </li>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['contact'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 20
        echo "</ul>
</nav>
<div>

</div>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "Messages/Contacts.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  64 => 20,  52 => 16,  49 => 15,  45 => 14,  31 => 2,  29 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% include \"layout.html.twig\"  %}
<script type=\"text/css\">
    #sidebar{
        position:fixed;
        padding-left: 0%;
        margin-left: 80%;
        padding-right: 0%;
        top: 0;
        bottom: 0;
    }
</script>
<nav id=\"sidebar\" >
<ul>
{% for contact in contacts %}
    <li>
        <a href=\"{{ path(\"conversation\" , {'contact': contact.idcontact }) }}\" class=\"btn btn-warning\">{{ contact.idContact }}</a>

    </li>
{% endfor %}
</ul>
</nav>
<div>

</div>
", "Messages/Contacts.html.twig", "C:\\laragon\\www\\kidsandfamily-master (1)\\kidsandfamily-master\\templates\\Messages\\Contacts.html.twig");
    }
}
