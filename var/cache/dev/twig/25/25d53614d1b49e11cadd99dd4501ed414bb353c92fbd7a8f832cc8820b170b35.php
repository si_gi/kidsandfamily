<?php

/* header.html.twig */
class __TwigTemplate_d71677bdd7a88247648600b5bf5cb0d69607d8dbcae1f6f9e0a9e9cfbe01c8b2 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "header.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "header.html.twig"));

        // line 1
        echo "<!-- Navigation -->
";
        // line 2
        $this->loadTemplate("layout.html.twig", "header.html.twig", 2)->display($context);
        // line 3
        echo "<nav class= \"navbar navbar-expand-md navbar-light fixed-top col-12\" style=\"background-color: #ffffff;\">
 <div class=\"container-fluid\">
\t <a class=\"navbar-brand\" href=\"#\"><img src=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("/img/logok&f.jpg"), "html", null, true);
        echo "\"></a>
   <a class=\"navbar-brand navbar-light\" href=\"#\">Réseau social de proximité</a>
\t <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarResponsive\">
\t\t <span class=\"navbar-toggler-icon\"></span>
\t </button>
\t <div class=\"collapse navbar-collapse\" id=\"navbarResponsive\">
\t\t <ul class=\"navbar-nav ml-auto\">
\t\t\t <li class=\"nav-item\">
\t\t\t\t <a class=\"nav-link\" href=\"#\">Accueil</a>
\t\t\t </li>
\t\t\t <li class=\"nav-item\">
\t\t\t\t <a class=\"nav-link\" href=\"#\">Profil</a>
\t\t\t </li>
\t\t\t <li class=\"nav-item\">
\t\t\t\t <a class=\"nav-link\" href=\"http://www.dev.kidsandfamily.fr\">Blog</a>
\t\t\t </li>
\t\t\t <li class=\"nav-item\">
\t\t\t\t <a class=\"nav-link\" href=\"#\">Inscription</a>
\t\t\t </li>
\t\t </ul>
\t </div>
 </div>
</nav>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  38 => 5,  34 => 3,  32 => 2,  29 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!-- Navigation -->
{% include \"layout.html.twig\" %}
<nav class= \"navbar navbar-expand-md navbar-light fixed-top col-12\" style=\"background-color: #ffffff;\">
 <div class=\"container-fluid\">
\t <a class=\"navbar-brand\" href=\"#\"><img src=\"{{asset(\"/img/logok&f.jpg\")}}\"></a>
   <a class=\"navbar-brand navbar-light\" href=\"#\">Réseau social de proximité</a>
\t <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarResponsive\">
\t\t <span class=\"navbar-toggler-icon\"></span>
\t </button>
\t <div class=\"collapse navbar-collapse\" id=\"navbarResponsive\">
\t\t <ul class=\"navbar-nav ml-auto\">
\t\t\t <li class=\"nav-item\">
\t\t\t\t <a class=\"nav-link\" href=\"#\">Accueil</a>
\t\t\t </li>
\t\t\t <li class=\"nav-item\">
\t\t\t\t <a class=\"nav-link\" href=\"#\">Profil</a>
\t\t\t </li>
\t\t\t <li class=\"nav-item\">
\t\t\t\t <a class=\"nav-link\" href=\"http://www.dev.kidsandfamily.fr\">Blog</a>
\t\t\t </li>
\t\t\t <li class=\"nav-item\">
\t\t\t\t <a class=\"nav-link\" href=\"#\">Inscription</a>
\t\t\t </li>
\t\t </ul>
\t </div>
 </div>
</nav>
", "header.html.twig", "C:\\laragon\\www\\kidsandfamily-master (1)\\kidsandfamily-master\\templates\\header.html.twig");
    }
}
