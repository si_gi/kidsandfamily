<?php

/* footer.html.twig */
class __TwigTemplate_cdb85b4bb07f9bede97431febf62dceb411461af73070cca498e7884e903a3f4 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "footer.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "footer.html.twig"));

        // line 1
        echo "<!--- Footer -->
<html>
<footer>
    <div class=\"container-fluid padding\">
        <div class=\"row text-center\">
            <div class=\"col-md-6\">
                <img class\"gif\" src=\"img/logo.png\" class=\"img-fluid\">
                <hr class=\"light\">
                <p>Qui sommes nous?</p>
                <p>Mentions légales</p>
                <p>Charte d'utilisation</p>
                <p>Devenir Ambassadeur</p>
            </div>
            <div class=\"col-md-6\">
                <img src=\"img/image3.jpg\" class\"img-fluid\">
                <hr class=\"light\">
            </div>
        </div>
    </div>
</footer>
</html>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "footer.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  29 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!--- Footer -->
<html>
<footer>
    <div class=\"container-fluid padding\">
        <div class=\"row text-center\">
            <div class=\"col-md-6\">
                <img class\"gif\" src=\"img/logo.png\" class=\"img-fluid\">
                <hr class=\"light\">
                <p>Qui sommes nous?</p>
                <p>Mentions légales</p>
                <p>Charte d'utilisation</p>
                <p>Devenir Ambassadeur</p>
            </div>
            <div class=\"col-md-6\">
                <img src=\"img/image3.jpg\" class\"img-fluid\">
                <hr class=\"light\">
            </div>
        </div>
    </div>
</footer>
</html>
", "footer.html.twig", "C:\\laragon\\www\\kidsandfamily-master (1)\\kidsandfamily-master\\templates\\footer.html.twig");
    }
}
