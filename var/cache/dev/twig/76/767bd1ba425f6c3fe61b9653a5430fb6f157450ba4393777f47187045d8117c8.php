<?php

/* accueil.html.twig */
class __TwigTemplate_aaa8ecfee0a6d1ad2c62fd56445b4e2118fe5717994f0694bf1a4ef639223c9a extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'fos_user_content' => [$this, 'block_fos_user_content'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "accueil.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "accueil.html.twig"));

        // line 1
        $this->loadTemplate("base.html.twig", "accueil.html.twig", 1)->display($context);
        // line 2
        echo "
";
        // line 3
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 4
        echo "

<body>
<div>

</div>
<div class=\"d-flex justify-content-center\">
  <div class=\"spinner-border\" role=\"status\">
    <span class=\"sr-only\">Loading...</span>
  </div>
</div>

<!-- Navigation -->
<nav class= \"navbar navbar-expand-md navbar-light fixed-top\" style=\"background-color: #ffffff;\">
 <div class=\"container-fluid\">
\t <a class=\"navbar-brand\" href=\"#\"><img src=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("/img/logok&f.jpg"), "html", null, true);
        echo "\"></a>
   <a class=\"navbar-brand navbar-light\" href=\"#\">Réseau social de proximité</a>
\t <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarResponsive\">
\t\t <span class=\"navbar-toggler-icon\"></span>
\t </button>
\t <div class=\"collapse navbar-collapse\" id=\"navbarResponsive\">
\t\t <ul class=\"navbar-nav ml-auto\">
\t\t\t <li class=\"nav-item\">
\t\t\t\t <a class=\"nav-link\" href=\"#\">Accueil</a>
\t\t\t </li>
\t\t\t <li class=\"nav-item\">
\t\t\t\t <a class=\"nav-link\" href=\"";
        // line 30
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("fos_user_security_login");
        echo "\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("layout.login", [], "FOSUserBundle"), "html", null, true);
        echo "</a>
\t\t\t </li>
\t\t\t <li class=\"nav-item\">
\t\t\t\t <a class=\"nav-link\" href=\"http://www.dev.kidsandfamily.fr\">Blog</a>
\t\t\t </li>
\t\t\t <li class=\"nav-item\">
\t\t\t\t <a class=\"nav-link\" href=\"#\">Inscription</a>
\t\t\t </li>
\t\t </ul>
\t </div>
 </div>
</nav>

<!--- Image Slider -->
<div id=\"slides\" class=\"carousel slide\" data-ride=\"carousel\" >
  <ol class=\"carousel-indicators text-center\">
    <li data-target=\"#slides\" data-slide-to=\"0\" class=\"active\"></li>
    <li data-target=\"#slides\" data-slide-to=\"1\"></li>
    <li data-target=\"#slides\" data-slide-to=\"2\"></li>
  </ol>
  <div class=\"carousel-inner\">
    <div class=\"carousel-item active\">
      <img src=\"/img/slide1.jpg\" class=\"d-block mw-100\">
\t\t\t<div class=\"carousel-caption\">
\t\t\t\t<h1 class=\"display-2\" class=\"Sketch_Gothic_School\">Bienvenue!</h1>
\t\t\t\t<h3 class=\"display-3\"></h3>
\t\t\t\t<a href=\"http://www.dev.kidsandfamily.fr\" class=\"btn btn-primary\">Site Principal</a>
\t\t\t</div>
    </div>
    <div class=\"carousel-item\">
      <img src=\"/img/slide2.jpg\" class=\"d-block w-100\">
    </div>
    <div class=\"carousel-item\">
      <img src=\"img/slide3.jpg\" class=\"d-block w-100\">
    </div>
  </div>
  <a class=\"carousel-control-prev\" href=\"#slides\" role=\"button\" data-slide=\"prev\">
    <span class=\"carousel-control-prev-icon\" aria-hidden=\"true\"></span>
    <span class=\"sr-only\">Previous</span>
  </a>
  <a class=\"carousel-control-next\" href=\"#slides\" role=\"button\" data-slide=\"next\">
    <span class=\"carousel-control-next-icon\" aria-hidden=\"true\"></span>
    <span class=\"sr-only\">Next</span>
  </a>
</div>
<div class=\"container-fluid padding\">
\t<div class=\"row picto text-center\">
\t\t<div class=\"col-12\">
      <img src=\"img/picto.jpg\">
\t\t</div>
\t</div>
</div>
<!--- Jumbotron
<div class=\"jumbotron\">
  <h1 class=\"display-5\">Ici se trouvera bientot un site</h5>
  <p class=\"lead\">Faites un tour et amusez vous, tant que ce ne sera pas fini il n'y aura pas grand chose a voir, mais bon.</p>
  <hr class=\"my-4\">
\t<a class=\"btn btn-warning btn-lg\" data-toggle=\"collapse\" href=\"#exemple\" role=\"button\" aria-expanded=\"false\" aria-controls=\"exemple\">En voir plus</a>
\t<div class=\"row\">
\t  <div class=\"col\">
\t    <div class=\"collapse\" id=\"exemple\">
\t      <div class=\"card card-body\">
\t        Spluch.
\t      </div>
\t    </div>
\t  </div>
\t</div>
</div> -->

<!--- Welcome Section -->
<div class=\"container-fluid padding\" style=\"background-color: #e6e6e6;\">
\t<div class=\"row welcome text-center\">
\t\t<div class=\"col-12\">
\t\t\t<h1 class=\"display-4\">Notre Volonté</h1>
\t\t\t<hr>
\t\t</div>
\t\t<div class=\"col-12\">
\t\t\t<p class=\"lead\">Faire vivre des experiences, recréer des moments de partage et de lien, pour les enfants, la famille et les lieux les accueillants.</p>
\t\t</div>
\t</div>
</div>

<div class=\"container-fluid padding\">
\t<div class=\"row slt text-center\">
\t\t<div class=\"col-12\">
\t\t\t<h1 class=\"display-4\">Notre Démarche</h1>
\t\t\t<hr>
\t\t</div>
\t\t<div class=\"col-12\">
\t\t\t<p class=\"lead\">Collaborer/S'entraider/Partager/Proposer/Recenser</p>
      <p class=\"lead\">Agir ensemble pour construire une vie meilleure en proximité</p>
      <img src=\"/img/Fonct.jpg\">
\t\t</div>
\t</div>
</div>

<!--- Three Column Section
<div class=\"container-fluid padding\">
<div class=\"row text-center bg-light padding\">
\t<div class=\"col-xs-12 col-sm-6 col-md-4\">
\t\t<img src=\"img/Embleme1.png\">
\t\t<h3>Pour l'Empereur</h3>
\t\t<p>Par Sa force divine.</p>
\t</div>
\t<div class=\"col-xs-12 col-sm-6 col-md-4\">
\t\t<img src=\"img/Embleme2.png\">
\t\t<h3>Pour Dorn</h3>
\t\t<p>Par son sacrfice et sa bravoure.</p>
\t</div>
\t<div class=\"col-xs-12 col-sm-12 col-md-4\">
\t\t<img src=\"img/Embleme.png\">
\t\t<h3>Et pour l'Imperium</h3>
\t\t<p>Pour qu'il retrouve sa gloire.</p>
\t</div>
</div>
<hr class=\"my-4\">
</div> -->

<div class=\"container-fluid padding\" style=\"background-color: #e6e6e6;\">
\t<div class=\"row welcome text-center\">
\t\t<div class=\"col-12\">
\t\t\t<h1 class=\"display-4\">Comment ca fonctionne?</h1>
\t\t\t<hr>
    </div>
  </div>
</div>

<!--- Two Column Section -->
<div class=\"container-fluid padding\" style=\"background-color: #e6e6e6;\">
\t<div class=\"row padding text-center\">
\t\t<div class=\"col-md-12 col-lg-6\">
\t\t\t<h2>Participer partout</h2>
      <hr>
\t\t\t<p>Bla bla bla bla bla bla bla bla bla bla bla bla bla bla</p>
\t\t\t<p>Bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla</p>
\t\t\t<br>
\t\t\t<a href=\"https://youtu.be/HJNz2QgSNsk\" class=\"btn btn-warning\">Rejoignez l'astra militarum</a>
\t\t</div>
\t\t<div class=\"col-lg-6\">
\t\t\t<img src=\"/img/GIF SITE INTERNET.gif\" class=\"img-fluid\">
\t\t</div>
\t</div>
</div>

<!--- Two Column Section -->
<div class=\"container-fluid padding\">
\t<div class=\"row padding text-center\">
\t\t<div class=\"col-lg-6\" >
\t\t\t<img class=\"img-fluid\" src=\"/img/visuel.png\">
\t\t</div>
\t\t<div class=\"col-lg-6\">
\t\t\t<h2>Collaborer</h2>
\t\t\t<hr>
\t\t\t<p>Vous aller aimer collaborer avec nous</p>
\t\t\t<p></p>
\t\t</div>
\t</div>
</div>

<!--- Fixed background
<figure>
\t<div class=\"fixed-wrap\">
\t\t<div id=\"fixed\">

\t\t</div>
\t</div>
</figure> -->

<!--- Emoji Section
<button class=\"fun btn-primary\" data-toggle=\"collapse\" data-target=\"#gif\">Cliquez, si vous l'osez</button>
<div id=\"gif\" class=\"collapse\">
\t<div class=\"container-fluid padding\" style=\"background-color: #e6e6e6;\">
\t\t<div class=\"row text-center\">
\t\t\t<div class=\"col-sm-12 col-md-12\">
\t\t\t\t<img class=\"gif\" src=\"img/tenor.gif\">
\t\t\t</div>
\t\t</div>
\t</div>
</div>-->

<!--- Meet the team -->
<div class=\"container-fluid padding\" style=\"background-color: #e6e6e6;>
\t<div class=\"row welcome text-center\">
\t\t<div class=\"col-12\">
\t\t\t<h1>Dites bonjour a l'actu:</h1>
\t\t</div>
\t\t<hr>
\t</div>
</div>

<!--- Cards -->
<div class=\"container-fluid padding\" style=\"background-color: #e6e6e6;\">
\t<div class=\"row padding\">
\t\t<div class=\"col-md-3\">
\t\t\t<div class=\"card\">
\t\t\t\t<img class=\"card-img-top\" src=\"img/image3.jpg\">
\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t<h4 class=\"card-title\">Actualité</h4>
\t\t\t\t\t<p>Salut l'actu</p>
\t\t\t\t\t<!--<a href=\"#\" class=\"btn btn-outline-secondary\">Tableau de chasse</a>-->
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"col-md-3\">
\t\t\t<div class=\"card\">
\t\t\t\t<img class=\"card-img-top\" src=\"img/image3.jpg\">
\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t<h4 class=\"card-title\">Actualité yo</h4>
\t\t\t\t\t<p>Oh boy l'actu</p>
\t\t\t\t\t<!--<a href=\"#\" class=\"btn btn-outline-secondary\">Tableau de chasse</a>-->
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"col-md-3\">
\t\t\t<div class=\"card\">
\t\t\t\t<img class=\"card-img-top\" src=\"img/image3.jpg\">
\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t<h4 class=\"card-title\">Actualité</h4>
\t\t\t\t\t<p>C'est un avion? c'est un oiseau? Non! C'est une actu!</p>
\t\t\t\t\t<a href='";
        // line 249
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("entries");
        echo "' class=\"btn btn-outline-secondary\">En savoir plus</a>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
    <div class=\"col-md-3\">
\t\t\t<div class=\"card\">
\t\t\t\t<img class=\"card-img-top\" src=\"img/image3.jpg\">
\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t<h4 class=\"card-title\">Actualité yo</h4>
\t\t\t\t\t<p>Oh boy l'actu</p>
\t\t\t\t\t<!--<a href=\"#\" class=\"btn btn-outline-secondary\">Tableau de chasse</a>-->
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
</div>

<!--- Connect -->
<div class=\"container-fluid padding\">
\t<div class=\"row text-center padding\">
\t\t<div class=\"col-12\">
\t\t\t<h2>Rejoignez nous!</h2>
\t\t</div>
\t\t<div class=\"col-12 social padding\">
\t\t\t<a href=\"#\"><i class=\"fab fa-facebook\"></i></a>
\t\t\t<a href=\"#\"><i class=\"fab fa-twitter\"></i></a>
\t\t\t<a href=\"#\"><i class=\"fab fa-instagram\"></i></a>
\t\t\t<a href=\"#\"><i class=\"fab fa-youtube\"></i></a>
\t\t</div>
\t</div>
</div>


<!--- Footer -->
<footer>
<div class=\"container-fluid padding\">
\t<div class=\"row text-center\">
\t\t<div class=\"col-md-4\">
\t\t\t<img src=\"img/image3.jpg\" class\"img-fluid\">
\t\t\t<hr class=\"light\">
\t\t</div>
\t\t<div class=\"col-md-4\">
\t\t\t<img class\"gif\" src=\"img/logo.png\" class=\"img-fluid\">
      \t<hr class=\"light\">
      <p>Qui sommes nous?</p>
\t\t\t<p>Mentions légales</p>
\t\t\t<p>Charte d'utilisation</p>
\t\t\t<p>Devenir Ambassadeur</p>
\t\t</div>
\t\t<div class=\"col-md-4\">
\t\t\t<img src=\"img/image3.jpg\" class\"img-fluid\">
\t\t\t<hr class=\"light\">
\t\t</div>
\t</div>
</div>
</footer>

";
        // line 306
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 306, $this->source); })()), "request", []), "hasPreviousSession", [])) {
            // line 307
            echo "    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 307, $this->source); })()), "session", []), "flashBag", []), "all", []));
            foreach ($context['_seq'] as $context["type"] => $context["messages"]) {
                // line 308
                echo "        ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($context["messages"]);
                foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                    // line 309
                    echo "            <div class=\"";
                    echo twig_escape_filter($this->env, $context["type"], "html", null, true);
                    echo "\">
                ";
                    // line 310
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans($context["message"], [], "FOSUserBundle"), "html", null, true);
                    echo "
            </div>
        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 313
                echo "    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['type'], $context['messages'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
        // line 315
        echo "
</body>
</html>
<<<<<<< HEAD
=======

>>>>>>> 1e1fd2700594593b73aab75dfd6e21f3539cc900
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "accueil.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  402 => 3,  385 => 315,  378 => 313,  369 => 310,  364 => 309,  359 => 308,  354 => 307,  352 => 306,  292 => 249,  68 => 30,  54 => 19,  37 => 4,  35 => 3,  32 => 2,  30 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% include \"base.html.twig\" %}

{% block fos_user_content %}{% endblock %}


<body>
<div>

</div>
<div class=\"d-flex justify-content-center\">
  <div class=\"spinner-border\" role=\"status\">
    <span class=\"sr-only\">Loading...</span>
  </div>
</div>

<!-- Navigation -->
<nav class= \"navbar navbar-expand-md navbar-light fixed-top\" style=\"background-color: #ffffff;\">
 <div class=\"container-fluid\">
\t <a class=\"navbar-brand\" href=\"#\"><img src=\"{{asset(\"/img/logok&f.jpg\")}}\"></a>
   <a class=\"navbar-brand navbar-light\" href=\"#\">Réseau social de proximité</a>
\t <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarResponsive\">
\t\t <span class=\"navbar-toggler-icon\"></span>
\t </button>
\t <div class=\"collapse navbar-collapse\" id=\"navbarResponsive\">
\t\t <ul class=\"navbar-nav ml-auto\">
\t\t\t <li class=\"nav-item\">
\t\t\t\t <a class=\"nav-link\" href=\"#\">Accueil</a>
\t\t\t </li>
\t\t\t <li class=\"nav-item\">
\t\t\t\t <a class=\"nav-link\" href=\"{{ path('fos_user_security_login') }}\">{{ 'layout.login'|trans({}, 'FOSUserBundle') }}</a>
\t\t\t </li>
\t\t\t <li class=\"nav-item\">
\t\t\t\t <a class=\"nav-link\" href=\"http://www.dev.kidsandfamily.fr\">Blog</a>
\t\t\t </li>
\t\t\t <li class=\"nav-item\">
\t\t\t\t <a class=\"nav-link\" href=\"#\">Inscription</a>
\t\t\t </li>
\t\t </ul>
\t </div>
 </div>
</nav>

<!--- Image Slider -->
<div id=\"slides\" class=\"carousel slide\" data-ride=\"carousel\" >
  <ol class=\"carousel-indicators text-center\">
    <li data-target=\"#slides\" data-slide-to=\"0\" class=\"active\"></li>
    <li data-target=\"#slides\" data-slide-to=\"1\"></li>
    <li data-target=\"#slides\" data-slide-to=\"2\"></li>
  </ol>
  <div class=\"carousel-inner\">
    <div class=\"carousel-item active\">
      <img src=\"/img/slide1.jpg\" class=\"d-block mw-100\">
\t\t\t<div class=\"carousel-caption\">
\t\t\t\t<h1 class=\"display-2\" class=\"Sketch_Gothic_School\">Bienvenue!</h1>
\t\t\t\t<h3 class=\"display-3\"></h3>
\t\t\t\t<a href=\"http://www.dev.kidsandfamily.fr\" class=\"btn btn-primary\">Site Principal</a>
\t\t\t</div>
    </div>
    <div class=\"carousel-item\">
      <img src=\"/img/slide2.jpg\" class=\"d-block w-100\">
    </div>
    <div class=\"carousel-item\">
      <img src=\"img/slide3.jpg\" class=\"d-block w-100\">
    </div>
  </div>
  <a class=\"carousel-control-prev\" href=\"#slides\" role=\"button\" data-slide=\"prev\">
    <span class=\"carousel-control-prev-icon\" aria-hidden=\"true\"></span>
    <span class=\"sr-only\">Previous</span>
  </a>
  <a class=\"carousel-control-next\" href=\"#slides\" role=\"button\" data-slide=\"next\">
    <span class=\"carousel-control-next-icon\" aria-hidden=\"true\"></span>
    <span class=\"sr-only\">Next</span>
  </a>
</div>
<div class=\"container-fluid padding\">
\t<div class=\"row picto text-center\">
\t\t<div class=\"col-12\">
      <img src=\"img/picto.jpg\">
\t\t</div>
\t</div>
</div>
<!--- Jumbotron
<div class=\"jumbotron\">
  <h1 class=\"display-5\">Ici se trouvera bientot un site</h5>
  <p class=\"lead\">Faites un tour et amusez vous, tant que ce ne sera pas fini il n'y aura pas grand chose a voir, mais bon.</p>
  <hr class=\"my-4\">
\t<a class=\"btn btn-warning btn-lg\" data-toggle=\"collapse\" href=\"#exemple\" role=\"button\" aria-expanded=\"false\" aria-controls=\"exemple\">En voir plus</a>
\t<div class=\"row\">
\t  <div class=\"col\">
\t    <div class=\"collapse\" id=\"exemple\">
\t      <div class=\"card card-body\">
\t        Spluch.
\t      </div>
\t    </div>
\t  </div>
\t</div>
</div> -->

<!--- Welcome Section -->
<div class=\"container-fluid padding\" style=\"background-color: #e6e6e6;\">
\t<div class=\"row welcome text-center\">
\t\t<div class=\"col-12\">
\t\t\t<h1 class=\"display-4\">Notre Volonté</h1>
\t\t\t<hr>
\t\t</div>
\t\t<div class=\"col-12\">
\t\t\t<p class=\"lead\">Faire vivre des experiences, recréer des moments de partage et de lien, pour les enfants, la famille et les lieux les accueillants.</p>
\t\t</div>
\t</div>
</div>

<div class=\"container-fluid padding\">
\t<div class=\"row slt text-center\">
\t\t<div class=\"col-12\">
\t\t\t<h1 class=\"display-4\">Notre Démarche</h1>
\t\t\t<hr>
\t\t</div>
\t\t<div class=\"col-12\">
\t\t\t<p class=\"lead\">Collaborer/S'entraider/Partager/Proposer/Recenser</p>
      <p class=\"lead\">Agir ensemble pour construire une vie meilleure en proximité</p>
      <img src=\"/img/Fonct.jpg\">
\t\t</div>
\t</div>
</div>

<!--- Three Column Section
<div class=\"container-fluid padding\">
<div class=\"row text-center bg-light padding\">
\t<div class=\"col-xs-12 col-sm-6 col-md-4\">
\t\t<img src=\"img/Embleme1.png\">
\t\t<h3>Pour l'Empereur</h3>
\t\t<p>Par Sa force divine.</p>
\t</div>
\t<div class=\"col-xs-12 col-sm-6 col-md-4\">
\t\t<img src=\"img/Embleme2.png\">
\t\t<h3>Pour Dorn</h3>
\t\t<p>Par son sacrfice et sa bravoure.</p>
\t</div>
\t<div class=\"col-xs-12 col-sm-12 col-md-4\">
\t\t<img src=\"img/Embleme.png\">
\t\t<h3>Et pour l'Imperium</h3>
\t\t<p>Pour qu'il retrouve sa gloire.</p>
\t</div>
</div>
<hr class=\"my-4\">
</div> -->

<div class=\"container-fluid padding\" style=\"background-color: #e6e6e6;\">
\t<div class=\"row welcome text-center\">
\t\t<div class=\"col-12\">
\t\t\t<h1 class=\"display-4\">Comment ca fonctionne?</h1>
\t\t\t<hr>
    </div>
  </div>
</div>

<!--- Two Column Section -->
<div class=\"container-fluid padding\" style=\"background-color: #e6e6e6;\">
\t<div class=\"row padding text-center\">
\t\t<div class=\"col-md-12 col-lg-6\">
\t\t\t<h2>Participer partout</h2>
      <hr>
\t\t\t<p>Bla bla bla bla bla bla bla bla bla bla bla bla bla bla</p>
\t\t\t<p>Bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla</p>
\t\t\t<br>
\t\t\t<a href=\"https://youtu.be/HJNz2QgSNsk\" class=\"btn btn-warning\">Rejoignez l'astra militarum</a>
\t\t</div>
\t\t<div class=\"col-lg-6\">
\t\t\t<img src=\"/img/GIF SITE INTERNET.gif\" class=\"img-fluid\">
\t\t</div>
\t</div>
</div>

<!--- Two Column Section -->
<div class=\"container-fluid padding\">
\t<div class=\"row padding text-center\">
\t\t<div class=\"col-lg-6\" >
\t\t\t<img class=\"img-fluid\" src=\"/img/visuel.png\">
\t\t</div>
\t\t<div class=\"col-lg-6\">
\t\t\t<h2>Collaborer</h2>
\t\t\t<hr>
\t\t\t<p>Vous aller aimer collaborer avec nous</p>
\t\t\t<p></p>
\t\t</div>
\t</div>
</div>

<!--- Fixed background
<figure>
\t<div class=\"fixed-wrap\">
\t\t<div id=\"fixed\">

\t\t</div>
\t</div>
</figure> -->

<!--- Emoji Section
<button class=\"fun btn-primary\" data-toggle=\"collapse\" data-target=\"#gif\">Cliquez, si vous l'osez</button>
<div id=\"gif\" class=\"collapse\">
\t<div class=\"container-fluid padding\" style=\"background-color: #e6e6e6;\">
\t\t<div class=\"row text-center\">
\t\t\t<div class=\"col-sm-12 col-md-12\">
\t\t\t\t<img class=\"gif\" src=\"img/tenor.gif\">
\t\t\t</div>
\t\t</div>
\t</div>
</div>-->

<!--- Meet the team -->
<div class=\"container-fluid padding\" style=\"background-color: #e6e6e6;>
\t<div class=\"row welcome text-center\">
\t\t<div class=\"col-12\">
\t\t\t<h1>Dites bonjour a l'actu:</h1>
\t\t</div>
\t\t<hr>
\t</div>
</div>

<!--- Cards -->
<div class=\"container-fluid padding\" style=\"background-color: #e6e6e6;\">
\t<div class=\"row padding\">
\t\t<div class=\"col-md-3\">
\t\t\t<div class=\"card\">
\t\t\t\t<img class=\"card-img-top\" src=\"img/image3.jpg\">
\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t<h4 class=\"card-title\">Actualité</h4>
\t\t\t\t\t<p>Salut l'actu</p>
\t\t\t\t\t<!--<a href=\"#\" class=\"btn btn-outline-secondary\">Tableau de chasse</a>-->
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"col-md-3\">
\t\t\t<div class=\"card\">
\t\t\t\t<img class=\"card-img-top\" src=\"img/image3.jpg\">
\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t<h4 class=\"card-title\">Actualité yo</h4>
\t\t\t\t\t<p>Oh boy l'actu</p>
\t\t\t\t\t<!--<a href=\"#\" class=\"btn btn-outline-secondary\">Tableau de chasse</a>-->
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"col-md-3\">
\t\t\t<div class=\"card\">
\t\t\t\t<img class=\"card-img-top\" src=\"img/image3.jpg\">
\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t<h4 class=\"card-title\">Actualité</h4>
\t\t\t\t\t<p>C'est un avion? c'est un oiseau? Non! C'est une actu!</p>
\t\t\t\t\t<a href='{{path(\"entries\")}}' class=\"btn btn-outline-secondary\">En savoir plus</a>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
    <div class=\"col-md-3\">
\t\t\t<div class=\"card\">
\t\t\t\t<img class=\"card-img-top\" src=\"img/image3.jpg\">
\t\t\t\t<div class=\"card-body\">
\t\t\t\t\t<h4 class=\"card-title\">Actualité yo</h4>
\t\t\t\t\t<p>Oh boy l'actu</p>
\t\t\t\t\t<!--<a href=\"#\" class=\"btn btn-outline-secondary\">Tableau de chasse</a>-->
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
</div>

<!--- Connect -->
<div class=\"container-fluid padding\">
\t<div class=\"row text-center padding\">
\t\t<div class=\"col-12\">
\t\t\t<h2>Rejoignez nous!</h2>
\t\t</div>
\t\t<div class=\"col-12 social padding\">
\t\t\t<a href=\"#\"><i class=\"fab fa-facebook\"></i></a>
\t\t\t<a href=\"#\"><i class=\"fab fa-twitter\"></i></a>
\t\t\t<a href=\"#\"><i class=\"fab fa-instagram\"></i></a>
\t\t\t<a href=\"#\"><i class=\"fab fa-youtube\"></i></a>
\t\t</div>
\t</div>
</div>


<!--- Footer -->
<footer>
<div class=\"container-fluid padding\">
\t<div class=\"row text-center\">
\t\t<div class=\"col-md-4\">
\t\t\t<img src=\"img/image3.jpg\" class\"img-fluid\">
\t\t\t<hr class=\"light\">
\t\t</div>
\t\t<div class=\"col-md-4\">
\t\t\t<img class\"gif\" src=\"img/logo.png\" class=\"img-fluid\">
      \t<hr class=\"light\">
      <p>Qui sommes nous?</p>
\t\t\t<p>Mentions légales</p>
\t\t\t<p>Charte d'utilisation</p>
\t\t\t<p>Devenir Ambassadeur</p>
\t\t</div>
\t\t<div class=\"col-md-4\">
\t\t\t<img src=\"img/image3.jpg\" class\"img-fluid\">
\t\t\t<hr class=\"light\">
\t\t</div>
\t</div>
</div>
</footer>

{% if app.request.hasPreviousSession %}
    {% for type, messages in app.session.flashBag.all %}
        {% for message in messages %}
            <div class=\"{{ type }}\">
                {{ message|trans({}, 'FOSUserBundle') }}
            </div>
        {% endfor %}
    {% endfor %}
{% endif %}

</body>
</html>
<<<<<<< HEAD
=======

>>>>>>> 1e1fd2700594593b73aab75dfd6e21f3539cc900
", "accueil.html.twig", "C:\\laragon\\www\\kidsandfamily-master (1)\\kidsandfamily-master\\templates\\accueil.html.twig");
    }
}
