<?php

/* blog.html.twig */
class __TwigTemplate_cf2685a445e2caa7033c157313643d4e0ce4cc9b32fdcc18a964b4fe3a9d5452 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'fos_user_content' => [$this, 'block_fos_user_content'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "blog.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "blog.html.twig"));

        // line 1
        $this->loadTemplate("base.html.twig", "blog.html.twig", 1)->display($context);
        // line 2
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 3
        echo "
<body>
<div class=\"container-fluid padding\">
  <div class=\"text-center\">
    <img src=\"img/background.jpg\">
  </div>
</div>

<div class=\"container-fluid padding\">
  <div class=\"row welcome text-center\">
    <div class=\"col-12\">
      <h1 class=\"display-4\">Bienvenu futur garde</h1>
      <hr>
    </div>
    <div class=\"col-12\">
      <p class=\"lead\">Bientot, peut etre trouveras tu ici une seconde page sur laquelle te renseigner.</p>
    </div>
  </div>
</div>

<figure>
  <div class=\"fixed-wrap\">
    <div id=\"fixed\">

    </div>
  </div>
</figure>

<div class=\"jumbotron\">
  <div class=\"container-fluid text-center\">
  <h1 class=\"display-5\">En attendant</h5>
  <p class=\"lead\">Récite donc ceci avec moi.</p>
  <hr class=\"my-4\">
  <a class=\"btn btn-warning btn-lg\" data-toggle=\"collapse\" href=\"#exemple\" role=\"button\" aria-expanded=\"false\" aria-controls=\"exemple\">En voir plus</a>
  <div class=\"row\">
    <div class=\"col\">
      <div class=\"collapse\" id=\"exemple\">
        <div class=\"card card-body\">
          <p>A spiritu dominatus,</p>
          <p>Domine, libra nos,</p>
          <p>From the lighting and the tempest,</p>
          <p>Our Emperor, deliver us.</p>
          <p>From plague, temptation and war,</p>
          <p>Our Emperor, deliver us,</p>
          <p>From the scourge of the Kraken,</p>
          <p>Our Emperor, deliver us.</p>
          <p></p>
          <p>From the blasphemy of the Fallen,</p>
          <p>Our Emperor, deliver us,</p>
          <p>From the begetting of daemons,</p>
          <p>Our Emperor, deliver us,</p>
          <p>From the curse of the mutant,</p>
          <p>Our Emperor, deliver us,</p>
          <p>A morte perpetua,</p>
          <p>Domine, libra nos.</p>
          <p>That thou wouldst bring them only death,</p>
          <p>That thou shouldst spare none,</p>
          <p>That thou shouldst pardon none</p>
          <p>We beseech thee, destroy them...</p>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
";
        // line 68
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 68, $this->source); })()), "request", []), "hasPreviousSession", [])) {
            // line 69
            echo "    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 69, $this->source); })()), "session", []), "flashBag", []), "all", []));
            foreach ($context['_seq'] as $context["type"] => $context["messages"]) {
                // line 70
                echo "        ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($context["messages"]);
                foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                    // line 71
                    echo "            <div class=\"";
                    echo twig_escape_filter($this->env, $context["type"], "html", null, true);
                    echo "\">
                ";
                    // line 72
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans($context["message"], [], "FOSUserBundle"), "html", null, true);
                    echo "
            </div>
        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 75
                echo "    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['type'], $context['messages'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
        // line 77
        echo "</body>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_fos_user_content($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "blog.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  145 => 2,  134 => 77,  127 => 75,  118 => 72,  113 => 71,  108 => 70,  103 => 69,  101 => 68,  34 => 3,  32 => 2,  30 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% include \"base.html.twig\" %}
{% block fos_user_content %}{% endblock %}

<body>
<div class=\"container-fluid padding\">
  <div class=\"text-center\">
    <img src=\"img/background.jpg\">
  </div>
</div>

<div class=\"container-fluid padding\">
  <div class=\"row welcome text-center\">
    <div class=\"col-12\">
      <h1 class=\"display-4\">Bienvenu futur garde</h1>
      <hr>
    </div>
    <div class=\"col-12\">
      <p class=\"lead\">Bientot, peut etre trouveras tu ici une seconde page sur laquelle te renseigner.</p>
    </div>
  </div>
</div>

<figure>
  <div class=\"fixed-wrap\">
    <div id=\"fixed\">

    </div>
  </div>
</figure>

<div class=\"jumbotron\">
  <div class=\"container-fluid text-center\">
  <h1 class=\"display-5\">En attendant</h5>
  <p class=\"lead\">Récite donc ceci avec moi.</p>
  <hr class=\"my-4\">
  <a class=\"btn btn-warning btn-lg\" data-toggle=\"collapse\" href=\"#exemple\" role=\"button\" aria-expanded=\"false\" aria-controls=\"exemple\">En voir plus</a>
  <div class=\"row\">
    <div class=\"col\">
      <div class=\"collapse\" id=\"exemple\">
        <div class=\"card card-body\">
          <p>A spiritu dominatus,</p>
          <p>Domine, libra nos,</p>
          <p>From the lighting and the tempest,</p>
          <p>Our Emperor, deliver us.</p>
          <p>From plague, temptation and war,</p>
          <p>Our Emperor, deliver us,</p>
          <p>From the scourge of the Kraken,</p>
          <p>Our Emperor, deliver us.</p>
          <p></p>
          <p>From the blasphemy of the Fallen,</p>
          <p>Our Emperor, deliver us,</p>
          <p>From the begetting of daemons,</p>
          <p>Our Emperor, deliver us,</p>
          <p>From the curse of the mutant,</p>
          <p>Our Emperor, deliver us,</p>
          <p>A morte perpetua,</p>
          <p>Domine, libra nos.</p>
          <p>That thou wouldst bring them only death,</p>
          <p>That thou shouldst spare none,</p>
          <p>That thou shouldst pardon none</p>
          <p>We beseech thee, destroy them...</p>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
{% if app.request.hasPreviousSession %}
    {% for type, messages in app.session.flashBag.all %}
        {% for message in messages %}
            <div class=\"{{ type }}\">
                {{ message|trans({}, 'FOSUserBundle') }}
            </div>
        {% endfor %}
    {% endfor %}
{% endif %}
</body>
", "blog.html.twig", "C:\\laragon\\www\\kidsandfamily-master (1)\\kidsandfamily-master\\templates\\blog.html.twig");
    }
}
