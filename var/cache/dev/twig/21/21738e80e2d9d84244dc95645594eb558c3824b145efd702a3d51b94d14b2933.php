<?php

/* Messages/Message.html.twig */
class __TwigTemplate_5be867866ea7df52e6c6ccd5809d12826c48a344d7974cdc06bb936bad96865a extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "Messages/Message.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "Messages/Message.html.twig"));

        // line 1
        $this->loadTemplate("layout.html.twig", "Messages/Message.html.twig", 1)->display($context);
        // line 2
        echo "<link rel=\"stylesheet\" href=\"https://www.w3schools.com/w3css/4/w3.css\">
<script type=\"text/css\">
    #sidebar{
        position:fixed;
        left: 0;
        top: 0;
        bottom: 0;
        border-left: #4c5367;
    }

</script>
<div \">
    <div class=\"w3-sidebar w3-light-grey w3-bar-block\" style=\"width:20%\">
<nav id=\"sidebar\" >
    <ul>
        ";
        // line 17
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["list_contact"]) || array_key_exists("list_contact", $context) ? $context["list_contact"] : (function () { throw new Twig_Error_Runtime('Variable "list_contact" does not exist.', 17, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["contact"]) {
            // line 18
            echo "            <li>
                <a href=\"";
            // line 19
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("conversation", ["contact" => twig_get_attribute($this->env, $this->source, $context["contact"], "idcontact", [])]), "html", null, true);
            echo "\" class=\"btn btn-warning\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["contact"], "idContact", []), "html", null, true);
            echo "</a>

            </li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['contact'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 23
        echo "    </ul>
</nav>

    </div>
<div>

</div>
    <div classe=\"w3-container w3-teal messagerie\"  style=\"margin-left:25%;\">
    ";
        // line 31
        if ((isset($context["messages"]) || array_key_exists("messages", $context))) {
            // line 32
            echo "        <ul style=\"list-style: none\">
        ";
            // line 33
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["messages"]) || array_key_exists("messages", $context) ? $context["messages"] : (function () { throw new Twig_Error_Runtime('Variable "messages" does not exist.', 33, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                // line 34
                echo "            ";
                if ((twig_get_attribute($this->env, $this->source, $context["message"], "receiver", []) == (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 34, $this->source); })()))) {
                    // line 35
                    echo "
                <li style=\"text-align: right;\" class=\"mymessage\">";
                    // line 36
                    echo twig_escape_filter($this->env, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 36, $this->source); })()), "html", null, true);
                    echo " ||";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["message"], "content", []), "html", null, true);
                    echo " </li>
            ";
                } else {
                    // line 38
                    echo "                <li style=\"text-align: left;\" class=\"othermessage\">";
                    echo twig_escape_filter($this->env, (isset($context["contact"]) || array_key_exists("contact", $context) ? $context["contact"] : (function () { throw new Twig_Error_Runtime('Variable "contact" does not exist.', 38, $this->source); })()), "html", null, true);
                    echo "||";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["message"], "content", []), "html", null, true);
                    echo "</li>
            ";
                }
                // line 40
                echo "        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 41
            echo "        </ul>
    ";
        }
        // line 43
        echo "    <div class=\"row\">
        <div class=\"col-sm-12 blog-main\">
            ";
        // line 45
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 45, $this->source); })()), "flashes", []));
        foreach ($context['_seq'] as $context["label"] => $context["messages"]) {
            // line 46
            echo "                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["messages"]);
            foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                // line 47
                echo "                    <div class=\"bg-";
                echo twig_escape_filter($this->env, $context["label"], "html", null, true);
                echo "\">
                        ";
                // line 48
                echo twig_escape_filter($this->env, $context["message"], "html", null, true);
                echo "
                    </div>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 51
            echo "            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['label'], $context['messages'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 52
        echo "
            ";
        // line 53
        if ((isset($context["form"]) || array_key_exists("form", $context))) {
            // line 54
            echo "                ";
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 54, $this->source); })()), 'form_start');
            echo "
                <div class=\"col-md-12\">
                    <div class=\"form-group col-md-4\">
                        ";
            // line 57
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 57, $this->source); })()), "content", []), 'row');
            echo "
                    </div>
                    <div class=\"form-group col-md-4 pull-right\">
                        ";
            // line 60
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 60, $this->source); })()), "submit", []), 'widget');
            echo "
                    </div>
                    ";
            // line 62
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 62, $this->source); })()), 'form_end');
            echo "
                </div>
            ";
        }
        // line 65
        echo "        </div>
    </div>
    </div>
</div>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "Messages/Message.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  179 => 65,  173 => 62,  168 => 60,  162 => 57,  155 => 54,  153 => 53,  150 => 52,  144 => 51,  135 => 48,  130 => 47,  125 => 46,  121 => 45,  117 => 43,  113 => 41,  107 => 40,  99 => 38,  92 => 36,  89 => 35,  86 => 34,  82 => 33,  79 => 32,  77 => 31,  67 => 23,  55 => 19,  52 => 18,  48 => 17,  31 => 2,  29 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% include \"layout.html.twig\" %}
<link rel=\"stylesheet\" href=\"https://www.w3schools.com/w3css/4/w3.css\">
<script type=\"text/css\">
    #sidebar{
        position:fixed;
        left: 0;
        top: 0;
        bottom: 0;
        border-left: #4c5367;
    }

</script>
<div \">
    <div class=\"w3-sidebar w3-light-grey w3-bar-block\" style=\"width:20%\">
<nav id=\"sidebar\" >
    <ul>
        {% for contact in list_contact %}
            <li>
                <a href=\"{{ path(\"conversation\" , {'contact': contact.idcontact }) }}\" class=\"btn btn-warning\">{{ contact.idContact }}</a>

            </li>
        {% endfor %}
    </ul>
</nav>

    </div>
<div>

</div>
    <div classe=\"w3-container w3-teal messagerie\"  style=\"margin-left:25%;\">
    {% if messages is defined %}
        <ul style=\"list-style: none\">
        {% for message in messages %}
            {% if message.receiver == user %}

                <li style=\"text-align: right;\" class=\"mymessage\">{{ user }} ||{{ message.content }} </li>
            {% else %}
                <li style=\"text-align: left;\" class=\"othermessage\">{{ contact }}||{{ message.content }}</li>
            {% endif %}
        {% endfor %}
        </ul>
    {% endif %}
    <div class=\"row\">
        <div class=\"col-sm-12 blog-main\">
            {% for label, messages in app.flashes %}
                {% for message in messages %}
                    <div class=\"bg-{{ label }}\">
                        {{ message }}
                    </div>
                {% endfor %}
            {% endfor %}

            {% if form is defined %}
                {{ form_start(form) }}
                <div class=\"col-md-12\">
                    <div class=\"form-group col-md-4\">
                        {{ form_row(form.content) }}
                    </div>
                    <div class=\"form-group col-md-4 pull-right\">
                        {{ form_widget(form.submit) }}
                    </div>
                    {{ form_end(form) }}
                </div>
            {% endif %}
        </div>
    </div>
    </div>
</div>", "Messages/Message.html.twig", "C:\\laragon\\www\\kidsandfamily-master (1)\\kidsandfamily-master\\templates\\Messages\\Message.html.twig");
    }
}
